﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;

namespace Interfaz
{
    public partial class DeleteProject : System.Web.UI.Page
    {
        string connectionString;
        private TaskBusiness taskBusiness;
        private ProyectBusiness projectBusiness;
        private CategoryBusiness categoryBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private RoleBusiness roleBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            //se crea la conexion
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;

            taskBusiness = new TaskBusiness(connectionString);
            projectBusiness = new ProyectBusiness(connectionString);
            categoryBusiness = new CategoryBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            roleBusiness = new RoleBusiness(connectionString);
        }//Page_Load

        protected void yes_button_Click(object sender, EventArgs e)
        {
            //se trae el ID de la tabla de la cual se quiere borrar
            //Ej: si quiero borrar de la tabla Project, se usaria projectIdSession
            //para saber que tengo que hacer los cambios en esa tabla
            string projectIdSession = (string)Session["idProject_Grid"];
            string categoryIdSession = (string)Session["idCategory_Grid"];
            string roleIdSession = (string)Session["idRole_Grid"];

            //se instancia el Dominio para poder hacer uso de los atributos de la clase
            Audit audit = new Audit();

            //se crea un numero aleatorio para el ID de la Auditoria
            Random random = new Random();

            //dependiendo de la tabla que estamos usando, asi se entrara
            //en cada IF. 
            if (projectBusiness.existsTheId(projectIdSession))
            {
                //se elimina el Proyecto
                projectBusiness.RemoveProyect(projectIdSession);

                //se llena el objeto Audit con los datos a insertar
                audit.ActionId = random.Next(100000, 999999);
                audit.ActionDate = DateTime.Today.ToString();
                audit.ActionMade = "Eliminar";
                audit.ActionTable = "Project";
                audit.ActionUser = (string)Session["user"];
                audit.ActionUserRole = logInBusiness.GetRole(
                    collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]));

                //se registra la auditoria
                auditBusiness.InsertAudit(audit);

                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageProject.aspx");
            }
            else if (categoryBusiness.existsTheId(categoryIdSession))
            {
                //se elimina el Proyecto
                categoryBusiness.RemoveCategory(categoryIdSession);

                //se llena el objeto Audit con los datos a insertar
                audit.ActionId = random.Next(100000, 999999);
                audit.ActionDate = DateTime.Today.ToString();
                audit.ActionMade = "Eliminar";
                audit.ActionTable = "Category";
                audit.ActionUser = (string)Session["user"];
                audit.ActionUserRole = logInBusiness.GetRole(
                    collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]));

                //se registra la auditoria
                auditBusiness.InsertAudit(audit);

                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageCategory.aspx");
            }
            else if (roleBusiness.existsTheId(Int32.Parse(roleIdSession)) == true)
            {
                roleBusiness.removeRole(Int32.Parse(roleIdSession));

                //se llena el objeto Audit con los datos a insertar
                audit.ActionId = random.Next(100000, 999999);
                audit.ActionDate = DateTime.Today.ToString();
                audit.ActionMade = "Eliminar";
                audit.ActionTable = "Role";
                audit.ActionUser = (string)Session["user"];
                audit.ActionUserRole = logInBusiness.GetRole(
                    collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]));

                //se registra la auditoria
                auditBusiness.InsertAudit(audit);

                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageRole.aspx");
            }
        }//yes_button_Click

        protected void no_button_Click(object sender, EventArgs e)
        {
            //se trae el ID de la tabla de la cual se quiere borrar
            //Ej: si quiero borrar de la tabla Project, se usaria projectIdSession
            //para saber que tengo que hacer los cambios en esa tabla
            string projectIdSession = (string)Session["idProject_Grid"];
            string categoryIdSession = (string)Session["idCategory_Grid"];
            string roleIdSession = (string)Session["idRole_Grid"];

            //dependiendo de la tabla que estamos usando, asi se entrara
            //en cada IF. 
            if (projectBusiness.existsTheId(projectIdSession))
            {
                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageProject.aspx");
            }
            else if (categoryBusiness.existsTheId(categoryIdSession))
            {
                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageCategory.aspx");
            }
            else if (roleBusiness.existsTheId(Int32.Parse(roleIdSession)))
            {
                //se redirecciona a la pagina para ver los cambios
                Response.Redirect("ManageRole.aspx");
            }
        }
    }
}