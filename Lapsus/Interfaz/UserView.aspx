﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserView.aspx.cs" Inherits="Interfaz.UserView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shorcut icon" href="img/LogoXXX.ico" />
    <title>Perfil Colaborador</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container" style="margin-top:70px;">
        <%--<div class="container center">
            <ul class="breadcrumb">
                <li><a href="UserView.aspx">Inicio</a></li>
            </ul>
        </div>--%>
    </div>
    <div class="container" style="margin-top:-40px;">
        <div class="container" style="margin-top:-40px;">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="UserView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30" />
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                        <a class="navbar-brand navbar-right" >      </a>
                        <a class="navbar-brand navbar-right"  href="#"><asp:Label ID="Label2" runat="server"  /></a>
                        
                    </div>
                </div>
            </div>
            <div class="container center" style="background-color:#E7E7E3; margin-top:15px" >
              <div style="margin-top:7px">
                <div class="row">               
                <ol class="breadcrumb" style="background-color:#E7E7E3;">
                <li class="active">Perfil colaborador</li>
                </ol>
                </div>                
            </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <br />
        <br />
        <div class="container center" style="margin-top:-40px;">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-body">
                        <form role="form" runat="server">
                            <div class="panel-heading" style="background-color:#E7E7E3;">
                                <h3 class="panel-title" style="background-color:#E7E7E3; color:#000;">
                               
                                    ¡Gestioná tus horas laboradas aquí!
                                   
                                </h3>
                            </div>
                            <br />
                            <br />
                            <div class="form-group">
                              
                                <asp:Button ID="btnRegistrarTarea" runat="server" Text="Administrar Horas" CssClass="btn btn-danger" Height="36px" OnClick="btnRegistrarTarea_Click" />

                               <%-- <asp:Button ID="btnVerTareas" runat="server" Text="Historial Horas" CssClass="btn btn-danger btn-lg" Height="36px" OnClick="btnVerTareas_Click" />
                               --%>
                            </div>
                            <br />
                            <br />
                            <%--<div class="form-group">
                                <asp:Button ID="btnVerTareas" runat="server" Text="Ver mis Horas" CssClass="btn btn-danger btn-lg" Height="36px" OnClick="btnVerTareas_Click" />
                            </div>--%>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="imgClock" style="height: 300px">
                        <img src="img/designer.jpg" style="float: inherit" class="img-thumbnail" alt="Cinque Terre" width="300" height="300" />
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>
    </div>
</body>
</html>
