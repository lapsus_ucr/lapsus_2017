﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;
using System.Data;

namespace Interfaz
{
    public partial class UserHoursRecord : System.Web.UI.Page
    {
        string connectionString;
        private TaskBusiness taskBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private ExtraHoursBusiness extraBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            taskBusiness = new TaskBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            extraBusiness = new ExtraHoursBusiness(connectionString);
        }

        protected void btn_Normales_Click(object sender, EventArgs e)
        {
            string idCardUuser = collaboratorBusiness.GetIdCardColaborator((string)Session["user"]);

            DataSet dataset = taskBusiness.getDatesByRange(txt_Desde.Text, txt_Hasta.Text, idCardUuser);

            gd_horas_normales.DataSource = dataset;
            gd_horas_normales.DataBind();
        }

        protected void btn_Extra_Click(object sender, EventArgs e)
        {
            string idCardUuser = collaboratorBusiness.GetIdCardColaborator((string)Session["user"]);

            DataSet dataset = extraBusiness.getHoursByRangeOfDates(txt_Desde.Text, txt_Hasta.Text, idCardUuser);

            gd_horas_normales.DataSource = dataset;
            gd_horas_normales.DataBind();
        }
    }
}