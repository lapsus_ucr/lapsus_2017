﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Domain;
using System.Web.Configuration;

namespace Interfaz
{
    public partial class UserView : System.Web.UI.Page
    {
        string connectionString;
        private ColaboratorBusiness collaboratorBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            collaboratorBusiness = new ColaboratorBusiness(connectionString);

            
            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);

            if (Session["user"] == null || Session["pass"]==null) {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btnRegistrarTarea_Click(object sender, EventArgs e)
        {
            //Response.Redirect("NewTask.aspx");
            Response.Redirect("MyTasks.aspx");
        }

      
    }
}