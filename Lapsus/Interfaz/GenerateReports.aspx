﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateReports.aspx.cs" Inherits="Interfaz.GenerateReports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"> 
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shorcut icon" href="img/LogoXXX.ico"/>
    <title>Generar Reportes</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet"/>
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">

        <div class="container">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
                <div class="container">
                    <div class="navbar-header" >
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="AdminView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30"/>
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                        <a class="navbar-brand navbar-right" >      </a>
                        <a class="navbar-brand navbar-right" href="#"><asp:Label ID="Label2" runat="server"  /></a>
                        
                    </div>
                </div>
            </div>
            <div class="container center" style="background-color:#E7E7E3; margin-top:15px" >
              <div style="margin-top:7px">
                <div class="row">               
                <ol class="breadcrumb" style="background-color:#E7E7E3;">
                <li><a href="AdminView.aspx">Perfil Administrador</a></li>
                <li class="active">Generar Reportes</li>
                </ol>
                </div>                
            </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <br />
        <br />

        <form role="form" runat="server">
            <div  class="container">
                <div  class="container center" style="margin-bottom: 10px;">
                    <h3>
                        <asp:Label runat="server" ID="lbl_rep">Reportes especiales: </asp:Label>
                    </h3>
                    <%--<asp:Button runat="server" ID="btnReport1" Text="Reporte"/>--%>
                    <div style="text-align:center;">
                        <asp:DropDownList AutoPostBack="false" ID="ddl_reportes" runat="server" Font-Size="Large" CssClass="form-control">
                            <asp:ListItem Value="HorasXProyecto" Text="Horas totales laboradas por proyecto"></asp:ListItem>
                            <asp:ListItem Value="MasHorasCol" Text="Horas totales de cada colaborador por proyecto"></asp:ListItem>
                            <asp:ListItem Value="RepGeneral" Text="Reporte general de horas laboradas"></asp:ListItem>
                        </asp:DropDownList>
                            
                        <div style="margin-top:15px;">
                            <asp:Button runat="server" ID="btnRep" Text="PDF" CssClass="btn btn-danger btn-sm" OnClick="btnRep_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnExcel" Text="Excel" CssClass="btn btn-info btn-sm" OnClick="btnExcel_Click"></asp:Button>
                        </div>
                    </div>
                </div>
            </div>
            <div  class="container">
                <div  class="container center" style="margin-bottom: 50px">
                    <h1>
                        <asp:Label runat="server" ID="lbl_title">
                            Colaboradores de la empresa XXX
                        </asp:Label>
                    </h1>
                    <p>
                        <asp:Label runat="server" ID="lbl_parra" Font-Size="Large"></asp:Label>
                    </p>
                </div>
                <br />                 
                <%--<div  runat="server" id="divFiltrado" class="col-lg-12"  style="margin-bottom: 50px" >
                    
                     <asp:Label runat="server" ID="lbFiltrado">Filtrar por:</asp:Label>
                      <br />   
                     <asp:TextBox ID="txtpyc"  type="text" runat="server" Font-Size="Large"  placeholder="Proyecto o Categoría" AutoCompleteType="Disabled" ></asp:TextBox>
                     <asp:Label runat="server" ID="lbDate">Fechas:</asp:Label>
                     <asp:TextBox ID="txtFechaInicio"  type="text" runat="server" Font-Size="Large"  placeholder="Desde" AutoCompleteType="Disabled" ></asp:TextBox>
                     <asp:TextBox ID="txtFechaFin"  type="text" runat="server" Font-Size="Large"  placeholder="Hasta" AutoCompleteType="Disabled" ></asp:TextBox>
                </div>--%>
                <br />             
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                          <asp:GridView ID="gd_colaboradores" runat="server" 
                                        AutoGenerateColumns="false" DataKeyNames="idCard"
                                        CssClass="table table-striped table-bordered table-hover">
                              <Columns>
                                  <asp:TemplateField HeaderText="Nombre">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_name" Text='<%# Bind("name") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_name" Text='<%# Bind("name") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField HeaderText="P. Apellido">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_1apellido" Text='<%# Bind("lastName1") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_1apellido" Text='<%# Bind("lastName1") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField HeaderText="S. Apellido">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_2apellido" Text='<%# Bind("lastName2") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_2apellido" Text='<%# Bind("lastName2") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Cédula">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_cedula" Text='<%# Bind("idCard") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_cedula" Text='<%# Bind("idCard") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Puesto">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_puesto" Text='<%# Bind("job") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_puesto" Text='<%# Bind("job") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField HeaderText="Rol">
                                      <EditItemTemplate>
                                          <asp:TextBox runat="server" ID="txt_rol" Text='<%# Bind("typeRole") %>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <ItemTemplate>
                                          <asp:Label runat="server"  ID="lbl_rol" Text='<%# Bind("typeRole") %>'></asp:Label>
                                      </ItemTemplate>
                                  </asp:TemplateField>

                                  <asp:TemplateField  HeaderText="Acción">
                                    <ItemTemplate >
                                        <asp:LinkButton ID="report_link" runat="server" Text="Reporte" CommandArgument='<%#Eval("idCard")%>' CommandName="report_link" OnCommand="report_link_Command"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              </Columns>
                          </asp:GridView>
                    </div>
                </div>

            </div>
            <div class="container center" style="margin-bottom: 50px; margin-top:-40px;">
                <div class="container center">
                    <h3>
                        <asp:Label runat="server" ID="h3_report_filter">
                            Criterios de filtrado
                        </asp:Label>
                    </h3>
                    
                    <asp:Label runat="server" ID="lbl_desde" Font-Size="Medium">
                            Desde
                    </asp:Label>
                    <asp:TextBox runat="server" ID="txt_desde" type="date" Font-Size="Medium"></asp:TextBox>
                    
                    <asp:Label runat="server" ID="lbl_hasta" Font-Size="Medium">
                            Hasta
                    </asp:Label>
                    <asp:TextBox runat="server" ID="txt_hasta" type="date" Font-Size="Medium"></asp:TextBox>
                    <%--<asp:Button ID="btnPrueba" runat="server" Text="Prueba" CssClass="btn btn-danger" Height="36px" OnClick="btnPrueba_Click" />--%>
                    <%--<asp:Button ID="btnRegisterExtraHours" runat="server" Text="Registrar Horas Extra" CssClass="btn btn-danger" Height="36px" OnClick="btnRegisterExtraHour_Click" />--%>    
                </div>
                <div class="container center" style="margin-top:15px;">
                    <div class="form-group">
                        <asp:Button ID="btnReport" runat="server" Text="Generar Reporte" CssClass="btn btn-default" Height="36px" OnClick="btnReport_Click" />
                        <asp:Button ID="btnCancelReport" runat="server" Text="Cancelar" CssClass="btn btn-danger" Height="36px" OnClick="btnCancelReport_Click"/>
                    </div>
                </div>
           </div>


        <%-- Aqui inician los GRIDVIEW que nunca se muestran sobre los REPORTES ESPECIALES.
            Se deben declarar y llenar para poder mostrarlos en PDF y Excel. --%>
            <asp:GridView ID="gd_report1" runat="server" 
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            PageSize="5" AllowPaging="false">
                <Columns>
                    <asp:BoundField DataField="Proyecto" HeaderText="Proyecto" HtmlEncode="true"/>
                    <asp:BoundField DataField="Total_Horas_Regulares" HeaderText="Total Horas Regulares" HtmlEncode="true"/>
                    <asp:BoundField DataField="Total_Horas_Extra" HeaderText="Total Horas Extra" HtmlEncode="true"/>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gd_report2" runat="server" 
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            PageSize="5" AllowPaging="false">
                <Columns>
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" HtmlEncode="true"/>
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" HtmlEncode="true"/>
                    <asp:BoundField DataField="Proyecto" HeaderText="Proyecto" HtmlEncode="true"/>
                    <asp:BoundField DataField="Total_Horas_Regulares" HeaderText="Total Horas Regulares" HtmlEncode="true"/>
                    <asp:BoundField DataField="Total_Horas_Extra" HeaderText="Total Horas Extra" HtmlEncode="true"/>
                </Columns>
            </asp:GridView>

            <asp:GridView ID="gd_report3" runat="server" 
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            PageSize="5" AllowPaging="false">
                <Columns>
                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" HtmlEncode="true"/>
                    <asp:BoundField DataField="Apellido" HeaderText="Apellido" HtmlEncode="true"/>
                    <asp:BoundField DataField="Cedula" HeaderText="Cédula" HtmlEncode="true"/>
                    <asp:BoundField DataField="Categoria" HeaderText="Categoría" HtmlEncode="true"/>
                    <asp:BoundField DataField="Proyecto" HeaderText="Proyecto" HtmlEncode="true"/>
                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" HtmlEncode="true"/>
                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" HtmlEncode="true"/>
                    <asp:BoundField DataField="HorasRegulares" HeaderText="Horas Regulares" HtmlEncode="true"/>
                    <asp:BoundField DataField="HorasExtra" HeaderText="Horas Extra" HtmlEncode="true"/>
                </Columns>
            </asp:GridView>

             <%-- Aqui Finalizan --%>

            <asp:GridView ID="gd_reports" runat="server" 
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            PageSize="5" AllowPaging="false">
                <Columns>
                    <asp:BoundField DataField="nameCategory" HeaderText="Categoría" HtmlEncode="true"/>
                    <asp:BoundField DataField="nameProject" HeaderText="Proyecto" HtmlEncode="true"/>
                    <asp:BoundField DataField="taskDescription" HeaderText="Descripción" HtmlEncode="true"/>
                    <asp:BoundField DataField="taskDate" HeaderText="Fecha" HtmlEncode="true"/>
                    <asp:BoundField DataField="taskRegularHours" HeaderText="Horas Regulares" HtmlEncode="true"/>
                    <asp:BoundField DataField="taskExtraHours" HeaderText="Horas Extra" HtmlEncode="true"/>
                </Columns>
            </asp:GridView>
            <div class="container center" style="margin-top:15px;">
                <div class="form-group">
                    <asp:Button ID="btnPDF" runat="server" Text="Exportar PDF" CssClass="btn btn-default" Height="36px" OnClick="btnPDF_Click" />
                    <asp:Button ID="btnExcell" runat="server" Text="Exportar Excel" CssClass="btn btn-info" Height="36px" OnClick="btnExcell_Click"/>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger" Height="36px" OnClick="btnCancelar_Click"/>
                </div>
            </div>  
            <div class="container center"">
                <asp:Label ID="lbl_noHoras" runat="server" CssClass="col-md-12 control-label text-danger"></asp:Label>
            </div>
        </form>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>

    </div>

</body>
</html>