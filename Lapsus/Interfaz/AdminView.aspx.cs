﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Domain;
using System.Web.Configuration;

namespace Interfaz
{
    public partial class AdminView : System.Web.UI.Page
    {
        string connectionString;
        private ColaboratorBusiness collaboratorBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            collaboratorBusiness = new ColaboratorBusiness(connectionString);

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);
        }

        protected void btn_manageCategory_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageCategory.aspx");
        }

        protected void btn_manageProject_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageProject.aspx");
        }

        protected void btn_manageRole_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageRole.aspx");
        }

        protected void btn_manageHours_Click(object sender, EventArgs e)
        {
            Response.Redirect("MyTaskAdmin.aspx");
        }

        protected void btn_generateReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("GenerateReports.aspx");
        }
    }
}