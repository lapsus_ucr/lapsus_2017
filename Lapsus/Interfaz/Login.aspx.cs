﻿using Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using System.Windows.Forms;

namespace Interfaz
{
    public partial class Login : System.Web.UI.Page
    {
        string connectionString;
        private LogInBusiness logInBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            logInBusiness = new LogInBusiness(connectionString);

            LoginLb.Visible = false;
        }

        protected void btn_ingresar_Click(object sender, EventArgs e)
        {
            
        }

        protected void btIngresar_Click(object sender, EventArgs e)
        {

            string usuario = TextBox1.Text;
            string contrasena = TextBox2.Text;

            LogIn usuarioEncontrado = logInBusiness.logIn(usuario, contrasena);

            string usuarioTabla = usuarioEncontrado.User;
            string contrasenaTabla = usuarioEncontrado.Pass;
            string cedulaTabla = usuarioEncontrado.IdCard;
            string role = logInBusiness.GetRole(usuarioEncontrado.IdCard);

            if (usuario == usuarioTabla && contrasena == contrasenaTabla)
            {
                Session["user"] = usuarioTabla;
                Session["pass"] = contrasenaTabla;

                if (Int32.Parse(role) == 1)
                {
                    Response.Redirect("AdminView.aspx");
                }
                else if (Int32.Parse(role) == 2)
                {
                    Response.Redirect("UserView.aspx");
                }
            }
            else
            {
                LoginLb.Visible = true;
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
        }

        protected void btCancelar_Click(object sender, EventArgs e)
        {
           TextBox1.Text="";
           TextBox2.Text="";
        }
    }
}