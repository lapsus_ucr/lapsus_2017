﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageRole.aspx.cs" Inherits="Interfaz.ManageRole" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
   <link rel="shorcut icon" href="img/LogoXXX.ico" />
    <title>Administrar Roles</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="container">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="AdminView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30" />
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                        <a class="navbar-brand navbar-right" >      </a>
                         <a class="navbar-brand navbar-right"  href="#"><asp:Label ID="Label2" runat="server"  /></a>
                        
                    </div>
                </div>
            </div>
            <div class="container center" style="background-color:#E7E7E3; margin-top:15px" >
              <div style="margin-top:7px">
                <div class="row">               
                <ol class="breadcrumb" style="background-color:#E7E7E3;">
                <li><a href="AdminView.aspx">Perfil Administrador</a></li>
                <li class="active">Administrar Roles</li>
                </ol>
                </div>                
            </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <form role="form" runat="server">
            <div  runat="server" id="formNewRole" class="container center"  style="margin-bottom: 15px; width:300px;">
            <h6 style="font-size: 2.5em; text-align: center; margin-top: 10px;">Registrá un rol</h6>
            <br />         
                 
            <%--<label style="font-size:20px; margin-top:10px;">Cédula</label>--%>
            <asp:TextBox ID="txt_idRole" CssClass="form-control" type="text" runat="server" Font-Size="Large"  placeholder="ID Rol" AutoCompleteType="Disabled"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txt_idRole_requerido" runat="server" 
                                        ControlToValidate="txt_idRole"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <asp:TextBox ID="txt_typeRole" CssClass="form-control" type="text" runat="server" Font-Size="Large"   placeholder="Tipo Rol" AutoCompleteType="Disabled"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txt_typeRole_requerido" runat="server" 
                                        ControlToValidate="txt_typeRole"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>

            <div style="margin-top: 10px; text-align: center;">
                <asp:Button ID="btn_Registrar" runat="server" Text="Registrar" CssClass="btn btn-default"  Height="36px" OnClick="btn_Registrar_Click"/>
                <asp:Button ID="btn_Cancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger"  Height="36px" OnClick="btn_Cancelar_Click" CausesValidation="false"/>
            </div>
            <div style="margin-top:20px;">
                <asp:Label ID="lblMensaje" runat="server" CssClass="labelMensajes"></asp:Label>
            </div>
                     
            </div>
            <div class="container">
                <div class="container center" style="margin-bottom: 50px">
                    <h1>
                        <asp:Label runat="server" ID="lbl_title">
                        Roles en el sistema
                        </asp:Label>
                    </h1>
                    <p>
                        <asp:Label runat="server" ID="lbl_parra">
                        
                        </asp:Label>
                    </p>
                </div>
                <br />
                
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gd_role" runat="server" Width="100%"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="false" DataKeyNames="idRole"
                            AutoGenerateDeleteButton="true" AutoGenerateEditButton="true"
                            OnRowCancelingEdit="gd_role_RowCancelingEdit" OnRowEditing="gd_role_RowEditing"
                            OnRowDeleting="gd_role_RowDeleting" OnRowUpdating="gd_role_RowUpdating">
                           <HeaderStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                             <Columns>
                                <asp:TemplateField HeaderText="ID Rol">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_idRole" runat="server"
                                            Text='<%# Bind("idRole")%>' type="number" ReadOnly="true">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_idRole" runat="server"
                                            Text='<%# Bind("idRole")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Tipo de Rol">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_typeRole" runat="server"
                                            Text='<%# Bind("typeRole")%>'>
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_typeRole" runat="server"
                                            Text='<%# Bind("typeRole")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                         
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
             <div class="container center" style="margin-bottom: 50px">
            <div class="form-group">
                              
                <asp:Button ID="btnRegisterRole" runat="server" Text="Registrar rol" CssClass="btn btn-danger" Height="36px" OnClick="btnRegisterRole_Click" />
                                                           
            </div>
            </div>
        </form>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>
    </div>
</body>
</html>