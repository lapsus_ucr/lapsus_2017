﻿using Business;
using Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Interfaz
{
    public partial class EditTask : System.Web.UI.Page
    {
        string connectionString;
        private CategoryBusiness categoryBusiness;
        private ProyectBusiness projectBusiness;
        private TaskBusiness taskBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        string txt_idTarea = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            taskBusiness = new TaskBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            categoryBusiness = new CategoryBusiness(connectionString);
            projectBusiness = new ProyectBusiness(connectionString);

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);

            categoryBusiness.GetCategoriesIds();
            projectBusiness.GetProyectsIds();

            LinkedList<Category> cat = categoryBusiness.GetCategoriesIds();
            LinkedList<Proyect> proy = projectBusiness.GetProyectsIds();

            foreach (Category category in cat)
            {
                ddl_Categoria.Items.Add(category.IdCategory);
            }

            foreach (Proyect project in proy)
            {
                ddl_Proyecto.Items.Add(project.IdProyect);
            }

            //DataSet dataset = taskBusiness.GetTask(collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));

            //taskBusiness.GetTask((string)Session["idTask"]);

            LinkedList<Tasks> ids = taskBusiness.GetTask((string)Session["idTask"]);

            if (!IsPostBack) {
                foreach (Tasks tasks in ids)
                {
                    txt_idTarea = tasks.IdTask;
                    ddl_Categoria.Text = tasks.IdCategory;
                    ddl_Proyecto.Text = tasks.IdProyect;
                    txt_Descripcion.Text = tasks.Description;
                    ddl_horas.Text = tasks.Hours;
                    txt_Cedula.Text = tasks.IdCardColaborator;
                }
            }
        }

        protected void btn_Editar_Click(object sender, EventArgs e)
        {
            Tasks newTask = new Tasks();

            newTask.IdTask = txt_idTarea;
            newTask.IdCategory = ddl_Categoria.SelectedItem.Text;
            newTask.IdProyect = ddl_Proyecto.SelectedItem.Text;
            newTask.Description = txt_Descripcion.Text;
            newTask.Date = "";
            newTask.Hours = ddl_horas.SelectedItem.Text;
            newTask.IdCardColaborator = txt_Cedula.Text;

            taskBusiness.EditTask(newTask, (string)Session["idTask"]);

            Response.Redirect("MyTasks.aspx");
            
        }
    }
}