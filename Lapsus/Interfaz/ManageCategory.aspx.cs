﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Data;
using Business;
using System.Web.Configuration;
using System.Data;
using System.Windows.Forms;



namespace Interfaz
{
    public partial class ManageCategory : System.Web.UI.Page
    {
        string connectionString;
        private CategoryBusiness categoryBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private ProyectBusiness projectBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            categoryBusiness = new CategoryBusiness(connectionString);
            projectBusiness = new ProyectBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);

            formNewCategory.Visible = false;

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);


            categoryBusiness.GetCategoriesIds();
            projectBusiness.GetProyectsIds();

            LinkedList<Category> cat = categoryBusiness.GetCategoriesNames();
            LinkedList<Proyect> proy = projectBusiness.GetAllProjectNames();

            if (!IsPostBack)
            {
                //si no hay categorías rgistradas
                //se imprime en interfaz
                if (getData() == false)
                {
                    lbl_title.Text = "Aún no hay categorías registradas";
                    lbl_parra.Text = "";
                }
                else
                {
                    categoryBusiness.GetAllCategoriesDataSet();
                }
            }

        }//Page_Load

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        //metodo encargado de cargar el gridView
        //si no hay registros de categorias para esa persona, el metodo retorna false
        //esto con el fin de cambiar la información de la página al cargar
        private bool getData()
        {

            categoryBusiness = new CategoryBusiness(connectionString);

            DataSet dataset = categoryBusiness.GetAllCategoriesDataSet();

            //si el dataset esta vacio, o sea, la tabla no tiene registros
            if (dataset.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                gd_category.DataSource = dataset;
                gd_category.DataBind();
                return true;
            }
        }//getData

        protected void gd_category_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gd_category.EditIndex = -1;

            getData();

        }//gd_category_RowCancelingEdit

        protected void gd_category_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gd_category.EditIndex = e.NewEditIndex;

            getData();
        }//gd_category_RowEditing

        protected void gd_category_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Session["idCategory_Grid"] = gd_category.DataKeys[e.RowIndex].Values["idCategory"].ToString();
            Response.Redirect("DeleteCategory.aspx");

        }//gd_category_RowDeleting

        protected void gd_category_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            categoryBusiness = new CategoryBusiness(connectionString);

            System.Web.UI.WebControls.TextBox txtIdCategory = gd_category.Rows[e.RowIndex].FindControl("txt_idCategory") as System.Web.UI.WebControls.TextBox;
            System.Web.UI.WebControls.TextBox txtNameCategory = gd_category.Rows[e.RowIndex].FindControl("txt_name") as System.Web.UI.WebControls.TextBox;

            Category category = new Category();
            Audit audit = new Audit();

            category.IdCategory = txtIdCategory.Text;
            category.Name = txtNameCategory.Text;
            category.AuditUser = (string)Session["user"];
            category.AuditDate= DateTime.Today.ToString();
            category.AuditAction = "Editar";

            categoryBusiness.EditCategory(category);

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Editar";
            audit.ActionTable = "Category";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            gd_category.EditIndex = -1;
            getData();

        }//gd_category_RowUpdating

        protected void btnRegistrarCategoria_Click(object sender, EventArgs e)
        {
            formNewCategory.Visible = true;
            btnRegisterCategory.Visible = false;

        }

        protected void btn_Cancelar_Click(object sender, EventArgs e)
        {
            btnRegisterCategory.Visible = true;
        }

        //Cuando se le da al boton "Registrar", se activa este evento
        //y se guardan los registro
        protected void btn_Registrar_Click(object sender, EventArgs e)
        {
            //se traen los datos ingresados por el Administrador
            //para poder guardarlos en la BD

            //se instancia el Dominio para poder hacer uso de los atributos de la clase
            Category category = new Category();
            Audit audit = new Audit();

            //se asignan los datos obtenidos a los atributos
            category.IdCategory = txt_id.Text;
            category.Name = txt_Name.Text;
            category.AuditUser= (string)Session["user"];
            category.AuditDate= DateTime.Today.ToString();
            category.AuditAction = "Insertar";

            //se trae el metodo y se pasa como parametro el objeto category
            //antes lleno
            categoryBusiness.InsertCategory(category);

            //se crea un numero aleatorio para el ID de la Auditoria
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Insertar";
            audit.ActionTable = "Category";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            //se redirecciona a la misma pagina para ver los cambios efectuados
            Response.Redirect("ManageCategory.aspx");
        }//btn_Registrar_Click
    }//class
}//namespace