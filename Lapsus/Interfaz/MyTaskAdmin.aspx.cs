﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace Interfaz
{
    public partial class MyTaskAdmin : System.Web.UI.Page
    {
        string connectionString;
        private TaskBusiness taskBusiness;
        private CategoryBusiness categoryBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private ProyectBusiness projectBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;
        private ExtraHoursBusiness extraHoursBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            categoryBusiness = new CategoryBusiness(connectionString);
            projectBusiness = new ProyectBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);
            extraHoursBusiness = new ExtraHoursBusiness(connectionString);
            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);

            formNewTask.Visible = false;
            btnRegisterExtraHours.Visible = false;

            categoryBusiness.GetCategoriesIds();
            projectBusiness.GetProyectsIds();

            LinkedList<Category> cat = categoryBusiness.GetCategoriesNames();
            LinkedList<Proyect> proy = projectBusiness.GetAllProjectNames();

            if (!IsPostBack)
            {
                //si la persona no tiene tareas registradas
                //se imprime en interfaz
                if (getData() == false)
                {
                    lbl_title.Text = "Aún no tenés tareas registradas";
                    lbl_parra.Text = "";
                    divFiltrado.Visible = false;
                }
                else
                {
                    divFiltrado.Visible = true;
                    taskBusiness.getAllIDTaskByIdCard((string)Session["user"]);
                }

                foreach (Category category in cat)
                {
                    ddl_Categoria.Items.Add(category.Name);
                }

                ddl_Categoria.Items.Insert(0, new ListItem("Categoría", ""));

                foreach (Proyect project in proy)
                {
                    ddl_Proyecto.Items.Add(project.Name);
                }

                ddl_Proyecto.Items.Insert(0, new ListItem("Proyecto", ""));

                ddl_horas.Items.Insert(0, new ListItem("Horas Regulares", "0"));

                ddl_horasExtra.Items.Insert(0, new ListItem("Horas Extra", "0"));
            }

        }//Page_Load

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        //metodo encargado de cargar el gridView
        //si no hay registros de tareas para esa persona, el metodo retorna false
        //esto con el fin de cambiar la información de la página al cargar
        private bool getData()
        {
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            taskBusiness = new TaskBusiness(connectionString);

            DataSet dataset = taskBusiness.getAllTaskByIdCard(collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));

            //si el dataset esta vacio, o sea, la persona no tiene tareas registradas
            if (dataset.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                gd_tareas.DataSource = dataset;
                gd_tareas.DataBind();
                return true;
            }
        }//getData

        //metodo encargado de obtener los datos del campo FECHA cada vez que 
        //se recarga la pagina
        private void getFirstDate(GridViewEditEventArgs e)
        {
            string firstDate = gd_tareas.Rows[e.NewEditIndex].Cells[5].Text;
        }//getFirstDate

        protected void gd_tareas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gd_tareas.EditIndex = -1;

            getData();

        }//gd_tareas_RowCancelingEdit

        protected void gd_tareas_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gd_tareas.EditIndex = e.NewEditIndex;

            getData();
        }//gd_tareas_RowEditing

        protected void gd_tareas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Session["idTask_Grid"] = gd_tareas.DataKeys[e.RowIndex].Values["idTask"].ToString();
            Response.Redirect("ConfirmDelete.aspx");

        }//gd_tareas_RowDeleting

        protected void gd_tareas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //Se crean los DropDownList que van a contener los DropDown del GridView
            DropDownList ddl_new = new DropDownList();
            DropDownList ddl_horas_extra = new DropDownList();
            DropDownList txtIdCategoria = new DropDownList();

            System.Web.UI.WebControls.TextBox txtIdTarea = gd_tareas.Rows[e.RowIndex].FindControl("txt_idTarea") as System.Web.UI.WebControls.TextBox;
            txtIdCategoria = gd_tareas.Rows[e.RowIndex].FindControl("txt_idCategoria") as DropDownList;
            ddl_new = gd_tareas.Rows[e.RowIndex].FindControl("ddl_project") as DropDownList;
            System.Web.UI.WebControls.TextBox txtDescripcion = gd_tareas.Rows[e.RowIndex].FindControl("txt_descripcion") as System.Web.UI.WebControls.TextBox;
            System.Web.UI.WebControls.TextBox txtFecha = gd_tareas.Rows[e.RowIndex].FindControl("txt_Fecha") as System.Web.UI.WebControls.TextBox;
            DropDownList txtHoras = gd_tareas.Rows[e.RowIndex].FindControl("ddl_horas") as DropDownList;
            ddl_horas_extra = gd_tareas.Rows[e.RowIndex].FindControl("ddl_horasExtra") as DropDownList;

            //Se instancia las clases para usar sus atributos
            Tasks task = new Tasks();
            Audit audit = new Audit();

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //A cada atributo se le asigna lo que el usuario ingreso
            //en el GridView
            task.IdTask = txtIdTarea.Text;
            task.IdCategory = txtIdCategoria.Text;
            task.IdProyect = ddl_new.Text;
            task.Description = txtDescripcion.Text;
            task.Hours = txtHoras.Text;
            task.TaskExtraHours = ddl_horas_extra.Text;
            task.IdCardColaborator = collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]);
            task.AuditUser = (string)Session["user"];
            task.AuditDate = DateTime.Today.ToString();
            task.AuditAction = "Editar";

            //se instancia el Business para usar sus metodos
            taskBusiness = new TaskBusiness(connectionString);

            //se utiliza el metodo EditTask para editar la Tarea
            //segun los datos dados por el usuario
            taskBusiness.EditTask(task, txtIdTarea.Text);

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Editar";
            audit.ActionTable = "Tasks";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            gd_tareas.EditIndex = -1;
            getData();

        }//gd_tareas_RowUpdating

        protected void btnRegisterHour_Click(object sender, EventArgs e)
        {
            //Response.Redirect("NewTask.aspx");
            formNewTask.Visible = true;
            btnRegisterHours.Visible = false;
            btnRegisterExtraHours.Visible = false;
        }

        protected void btnRegisterExtraHour_Click(object sender, EventArgs e)
        {

        }

        protected void btn_Registrar_Click(object sender, EventArgs e)
        {
            //se obtienen los datos del formulario y se colocan en variables
            string nameCategoria = ddl_Categoria.SelectedItem.Text;
            string nameProyecto = ddl_Proyecto.SelectedItem.Text;
            string descripcion = txt_Descripcion.Text;
            string fecha = DateTime.Today.ToString();
            string horas = ddl_horas.SelectedItem.Value;
            string horasExtra = ddl_horasExtra.SelectedItem.Value;
            string cedula = collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]);

          //se parsea el string a double
          //se debe deinir la Cultura, pues al parsear de string a double, se pierde la coma
          //o punto y queda como un numero entero
          double horasRegulares = Convert.ToDouble(horas.ToString(), CultureInfo.InvariantCulture);
            double horasExtraConver = Convert.ToDouble(horasExtra.ToString(), CultureInfo.InvariantCulture);

            if (horas == "0" && horasExtra == "0")
            {
                lblMensaje.Text = "Se debe registrar al menos un tipo de hora";
            }
            else if (horasExtraConver > (24 - horasRegulares))
            {
                lblMensaje.Text = "No se pueden ingresar más de 24 horas";
            }
            else
            {
                //se instancia el Business
                TaskBusiness tBusiness = new TaskBusiness(connectionString);

                //se instancia el Domain
                Tasks task = new Tasks();
                Audit audit = new Audit();
                ExtraHours extraHours = new ExtraHours();

                //se crea una variables de tipo Random
                //para su posterior uso
                Random random = new Random();


                //se llena el objeto Tasks con los datos a insertar
                task.IdTask = random.Next(100000, 999999).ToString();
                task.IdCategory = nameCategoria;
                task.IdProyect = nameProyecto;
                task.Description = descripcion;
                task.Date = fecha;
                task.Hours = horas;
                task.TaskType = "Regulares";
                task.IdCardColaborator = cedula;
                task.AuditUser = (string)Session["user"];
                task.AuditDate = DateTime.Today.ToString();
                task.AuditAction = "Insertar";
                task.TaskExtraHours = horasExtra;

                //se llama al Business y se inserta la tarea nueva
                tBusiness.InsertTask(task);

                //se llena el objeto Audit con los datos a insertar
                audit.ActionId = random.Next(100000, 999999);
                audit.ActionDate = DateTime.Today.ToString();
                audit.ActionMade = "Insertar";
                audit.ActionTable = "Tasks";
                audit.ActionUser = (string)Session["user"];
                audit.ActionUserRole = logInBusiness.GetRole(
                    collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]));

                //se llama al Business y se inserta la nueva Auditoria
                auditBusiness.InsertAudit(audit);

                //se vuelve a la misma pagina para ver los cambios efectuados
                Response.Redirect("MyTaskAdmin.aspx");
            }
        }//btn_Registrar_Click

        protected void btn_Cancelar_Click(object sender, EventArgs e)
        {
            formNewTask.Visible = false;
            btnRegisterHours.Visible = true;
            btnRegisterExtraHours.Visible = false;
        }

        //Con este metodo se carga el dropdowlist que contiene el nombre de los 
        //proyectos.
        //Esto a la hora que se pulsa sobre el boton "Edit" del GridVview
        protected void gd_tareas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //obtengo el ID de la tarea a editar. Esto con el fin de utilizarlo mas
            //adelante para obtener el nombre de su proyecto respectivo
            System.Web.UI.WebControls.TextBox idTask = (System.Web.UI.WebControls.TextBox)e.Row.FindControl("txt_idTarea");

            if (e.Row.RowType == DataControlRowType.DataRow && gd_tareas.EditIndex == e.Row.RowIndex)
            {
                DropDownList projects = (DropDownList)e.Row.FindControl("ddl_project");
                DropDownList categories = (DropDownList)e.Row.FindControl("txt_idCategoria");

                LinkedList<Proyect> proy = projectBusiness.GetAllProjectNames();
                LinkedList<Category> cat = categoryBusiness.GetCategoriesNames();

                foreach (Proyect project in proy)
                {
                    projects.Items.Add(project.Name);
                }//foreach

                foreach (Category category in cat)
                {
                    categories.Items.Add(category.Name);
                }//foreach

                //con esta linea hago que el DropDownList muestre el valor que yo quiero de 
                //primero. Y no el que esta de primero en la fila.
                projects.SelectedValue = taskBusiness.getTaskNameById(idTask.Text);
                categories.SelectedValue = taskBusiness.getCategoryNameByTaskId(idTask.Text);
            }//if
        }//

        //evento que se activa cuando se pulsa sobre el link de duplicar
        protected void duplicate_link_Command(object sender, CommandEventArgs e)
        {
            TaskBusiness taskBusiness = new TaskBusiness(connectionString);
            //se pregunta si el comando es el indicado en el Link del GridView
            if (e.CommandName == "duplicate_field")
            {

                //se obtiene el ID de la fila a editar
                //esto se logra con el CommandoArgument declarado en el Link
                int index_row = Int32.Parse(e.CommandArgument.ToString());

                //se crea una Lista y se llena con la informacion de la tarea seleccionada
                //en el gridview, esto con el fin de duplicarla
                LinkedList<Tasks> tasks = taskBusiness.GetTaskForDuplicate(index_row.ToString());

                //Se instancian las clases para usar sus atributos
                Tasks task = new Tasks();
                Audit audit = new Audit();

                //se crea una variables de tipo Random
                //para su posterior uso
                Random random = new Random();

                //se recorre la lista de tareas y se asignan los valores al objeto ask
                foreach (Tasks myTask in tasks)
                {
                    task.IdCategory = myTask.IdCategory;
                    task.IdProyect = myTask.IdProyect;
                    task.Description = myTask.Description;
                    task.Date = myTask.Date;
                    task.Hours = myTask.Hours;
                    task.TaskExtraHours = myTask.TaskExtraHours;
                }

                //A cada atributo se le asigna lo que el usuario ingreso
                //en el GridView
                //se llena el objeto Tasks con los datos a insertar
                task.IdTask = random.Next(100000, 999999).ToString();
                task.IdCardColaborator = collaboratorBusiness.GetIdCardColaborator(
                                         (string)Session["user"]);
                task.AuditUser = (string)Session["user"];
                task.AuditDate = DateTime.Today.ToString();
                task.AuditAction = "Duplicar";

                taskBusiness.InsertTask(task);

                //se vuelve a la misma pagina para ver los cambios efectuados
                Response.Redirect("MyTasks.aspx");
            }
        }//duplicate_link_Command

        protected void btn_buscar_Click(object sender, EventArgs e)
        {
            taskBusiness = new TaskBusiness(connectionString);

            DataSet dataset = taskBusiness.getAllTaskByIdCard(collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));

            if (txtpyc.Text == "" && txtFechaInicio.Text == "" && txtFechaFin.Text == "")
            {
                if (dataset.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen horas registradas";
                    gd_tareas.Visible = false;
                }
                else
                {
                    gd_tareas.Visible = true;
                    gd_tareas.DataSource = dataset;
                    gd_tareas.DataBind();
                }
            }
            else if (txtpyc.Text != "" && txtFechaInicio.Text == "" && txtFechaFin.Text == "")
            {
                DataSet datasetLike = taskBusiness.GetTaskLike(txtpyc.Text,
                collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));
                if (datasetLike.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen registros con la categoría: " + txtpyc.Text;
                    gd_tareas.Visible = false;
                }
                else
                {
                    gd_tareas.Visible = true;
                    gd_tareas.DataSource = datasetLike;
                    gd_tareas.DataBind();
                }
            }
            else if (txtpyc.Text == "" && txtFechaInicio.Text != "" && txtFechaFin.Text != "")
            {
                if (dataset.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen registros en el rango de fechas indicado";
                    gd_tareas.Visible = false;
                }
                else
                {
                    gd_tareas.Visible = true;
                    DataSet dataRange = taskBusiness.getDatesByRange2(txtFechaInicio.Text, txtFechaFin.Text,
                        collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));
                    gd_tareas.DataSource = dataRange;
                    gd_tareas.DataBind();
                    lbl_filter.Text = "";
                }
            }
        }
    }//CLASS
}//namespace