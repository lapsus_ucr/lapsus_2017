﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Domain;
using System.Web.Configuration;
using System.Data;
using System.Windows.Forms;

namespace Interfaz
{
    public partial class ManageProject : System.Web.UI.Page
    {
        string connectionString;
        private ProyectBusiness projectBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;

        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            projectBusiness = new ProyectBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);

            formNewProject.Visible = false;

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);


            if (!IsPostBack)
            {
                //si la persona no tiene tareas registradas
                //se imprime en interfaz
                if (getData() == false)
                {
                    lbl_title.Text = "Aún no hay proyectos registrados";
                    lbl_parra.Text = "";
                }
                else
                {
                    projectBusiness.GetAllProjectsDataSet();
                }

                ddl_estados.Items.Insert(0, new ListItem("Estados", ""));
                d.Visible = false;

            }

        }//Page_Load

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        //metodo encargado de cargar el gridView
        //si no hay registros de tareas para esa persona, el metodo retorna false
        //esto con el fin de cambiar la información de la página al cargar
        private bool getData()
        {

            projectBusiness = new ProyectBusiness(connectionString);

            DataSet dataset = projectBusiness.GetAllProjectsDataSet();

            //si el dataset esta vacio, o sea, la tabla no tiene registros
            if (dataset.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                gd_project.DataSource = dataset;
                gd_project.DataBind();
                return true;
            }
        }//getData



        protected void gd_project_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gd_project.EditIndex = -1;

            getData();

        }//gd_project_RowCancelingEdit

        protected void gd_project_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gd_project.EditIndex = e.NewEditIndex;

            getData();
        }//gd_project_RowEditing

        protected void gd_project_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Session["idProject_Grid"] = gd_project.DataKeys[e.RowIndex].Values["idProject"].ToString();
            Response.Redirect("DeleteProject.aspx");

        }//gd_proect_RowDeleting

        protected void gd_project_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            projectBusiness = new ProyectBusiness(connectionString);

            DropDownList ddl_new = new DropDownList();

            System.Web.UI.WebControls.TextBox txtIdProject = gd_project.Rows[e.RowIndex].FindControl("txt_idProject") as System.Web.UI.WebControls.TextBox;
            System.Web.UI.WebControls.TextBox txtNameProject = gd_project.Rows[e.RowIndex].FindControl("txt_name") as System.Web.UI.WebControls.TextBox;
            DropDownList txtEstados = gd_project.Rows[e.RowIndex].FindControl("ddl_estados") as System.Web.UI.WebControls.DropDownList;

            Proyect project = new Proyect();

            project.IdProyect = txtIdProject.Text;
            project.Name = txtNameProject.Text;
            project.Status = txtEstados.Text;
            project.AuditUser= (string)Session["user"];
            project.AuditDate= DateTime.Today.ToString();
            project.AuditAction = "Editar";

            projectBusiness.EditProyect(project);

            gd_project.EditIndex = -1;
            getData();

        }//gd_categorias_RowUpdating
        protected void btn_Registrar_Click(object sender, EventArgs e)
        {
            //se instancia el Dominio para poder hacer uso de los atributos de la clase
            Proyect project = new Proyect();
            Audit audit = new Audit();

            //se asignan los datos obtenidos a los atributos
            project.IdProyect = txt_id.Text;
            project.Name = txt_Name.Text;
            project.AuditUser = (string)Session["user"];
            project.AuditDate = DateTime.Today.ToString();
            project.AuditAction = "Insertar";

            projectBusiness.InsertProyect(project);

            //se crea un numero aleatorio para el ID de la Auditoria
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Insertar";
            audit.ActionTable = "Project";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            //se redirecciona a la misma pagina para ver los cambios efectuados
            Response.Redirect("ManageProject.aspx");
        }

        protected void btn_Cancelar_Click(object sender, EventArgs e)
        {
            formNewProject.Visible = false;
            btnRegisterProject.Visible = true;
        }

        protected void btnRegistrarProyecto_Click(object sender, EventArgs e)
        {
            formNewProject.Visible = true;
            btnRegisterProject.Visible = false;
        }//btnRegistrarProyecto_Click
    }//Class
}//Namespace