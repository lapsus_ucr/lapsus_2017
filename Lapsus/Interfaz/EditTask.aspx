﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTask.aspx.cs" Inherits="Interfaz.EditTask" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shorcut icon" href="img/LogoXXX.ico" />
    <title>Edit Task</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>
</head>
<body>

    <div class="container">

        <div class="container">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="UserView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30" />
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                        <a class="navbar-brand navbar-right" >      </a>
                        <a class="navbar-brand navbar-right"  href="#"><asp:Label ID="Label2" runat="server"  /></a>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <br />
        <br />





        <div class="container center">
            <div class="row">
                <div class="col-md-6">

                    <div class="panel-body">
                        <form role="form" runat="server">
                            <div class="panel-heading" style="background-color:#E7E7E3;">
                                <h3 class="panel-title" style="background-color:#E7E7E3; color:#000;">¡Modifica una tarea registrada!
                                </h3>
                            </div>
                            <%--<div class="form-group">
                                <label>ID Tarea</label>
                                <asp:TextBox ID="txt_idTarea" CssClass="form-control" type="number" runat="server" Font-Size="Large" Style="text-align: center"></asp:TextBox>
                            </div>--%>
                            <div class="form-group">
                                <label>ID Categoria</label>
                                <asp:DropDownList ID="ddl_Categoria" runat="server" Font-Size="Large" CssClass="btn btn-inverse"></asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label>ID Proyecto</label>
                                <asp:DropDownList ID="ddl_Proyecto" runat="server" Font-Size="Large" CssClass="btn btn-inverse"></asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label>Descripcion</label>
                                <asp:TextBox ID="txt_Descripcion" CssClass="form-control" type="text" runat="server" Font-Size="Large" Style="text-align: center"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Horas trabajadas</label>
                                <asp:DropDownList ID="ddl_horas" runat="server" Font-Size="Large" CssClass="btn btn-inverse">
                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                    <asp:ListItem Value="1,5" Text="1,5"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                    <asp:ListItem Value="2,5" Text="2,5"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                    <asp:ListItem Value="3,5" Text="3,5"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                    <asp:ListItem Value="4,5" Text="4,5"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                    <asp:ListItem Value="5,5" Text="5,5"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                    <asp:ListItem Value="6,5" Text="6,5"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                    <asp:ListItem Value="7,5" Text="7,5"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label>Cédula</label>
                                <asp:TextBox ID="txt_Cedula" CssClass="form-control" type="text" runat="server" Font-Size="Large" ReadOnly="true" Style="text-align: center"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btn_Editar" runat="server" Text="Actualizar" CssClass="btn btn-danger" OnClick="btn_Editar_Click" Height="36px" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="imgClock" style="height: 300px">
                        <img src="img/img4.jpg" style="float: inherit" class="img-thumbnail" alt="Cinque Terre" width="300" height="300" />
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>
    </section>
</body>
</html>
