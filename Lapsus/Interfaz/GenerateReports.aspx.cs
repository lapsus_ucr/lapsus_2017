﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;
using System.Data;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text.html.simpleparser;
using System.Drawing;

namespace Interfaz
{
    
    public partial class GenerateReports : System.Web.UI.Page
    {
        string connectionString;
        private ColaboratorBusiness collaboratorBusiness;
        private TaskBusiness taskBusiness;

        protected void Page_Load(object sender, EventArgs e)

        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            taskBusiness = new TaskBusiness(connectionString);

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);
            btnPDF.Visible = false;
            btnExcell.Visible = false;
            btnCancelar.Visible = false;
            btnCancelReport.Visible = false;
            gd_report1.Visible = false;
            gd_report2.Visible = false;
            gd_report3.Visible = false;

            //DataSet dataset1 = taskBusiness.reporteUno();
            //gd_report1.DataSource = dataset1;
            //gd_report1.DataBind();

            //DataTable tbl = dataset1.Tables["datos_reporte_1"];

            //foreach (DataRow current in tbl.Rows) {
            //    MessageBox.Show(current["Proyecto"].ToString());
            //}

            //se hacen invisibles los campos de filtrado, pues al momento de que cargue la pagina
            //aun no se ha seleccionado ningun colaborador
            h3_report_filter.Visible = false;
            lbl_desde.Visible = false;
            lbl_hasta.Visible = false;
            txt_desde.Visible = false;
            txt_hasta.Visible = false;
            btnReport.Visible = false;

            //se llena el gridview pero solo una vez 
            if (!IsPostBack) {
                DataSet dataset = collaboratorBusiness.GetAllCollaborators();
                gd_colaboradores.DataSource = dataset;
                gd_colaboradores.DataBind();
            }
        }

        //evento que se desencadena cuando se le da sobre el link presente en cada
        //colaborador en el gridView
        protected void report_link_Command(object sender, CommandEventArgs e)
        {
            //se pregunta si el comando es el indicado en el Link del GridView
            if (e.CommandName == "report_link")
            {
                //se sube a sesion la cedula de la persona elegida para el reporte
                //la cedula se obtiene gracias al argumento de la fila del gridview
                //Esto se especifica en el CommandArgument en la etiqueta asp:link
                Session["idCard_Report"] = e.CommandArgument.ToString();

                //se habilitan los campos para que el admnistrador pueda filtrar
                //por fechas
                h3_report_filter.Visible = true;
                lbl_desde.Visible = true;
                lbl_hasta.Visible = true;
                txt_desde.Visible = true;
                txt_hasta.Visible = true;
                btnReport.Visible = true;
                gd_colaboradores.Visible = false;
                btnCancelReport.Visible = true;

                lbl_parra.Text = "Colaborador: " + collaboratorBusiness.CollaboratorFullName(Session["idCard_Report"].ToString()); ;
            }//if
        }//report_link_Command

        //evento al darle click sobre el boton de reporte
        protected void btnReport_Click(object sender, EventArgs e)
        {
            //se habilitan los campos para que el admnistrador pueda filtrar
            //por fechas
            h3_report_filter.Visible = true;
            lbl_desde.Visible = true;
            lbl_hasta.Visible = true;
            txt_desde.Visible = true;
            txt_hasta.Visible = true;
            btnReport.Visible = true;
            btnCancelar.Visible = true;
            btnCancelReport.Visible = true;

            DataSet dataset= taskBusiness.getDatesByRange2(txt_desde.Text, txt_hasta.Text, Session["idCard_Report"].ToString());

            if (dataset.Tables[0].Rows.Count == 0)
            {
                btnPDF.Visible = false;
                btnExcell.Visible = false;
                gd_reports.Visible = false;
                lbl_noHoras.Visible = true;
                btnCancelar.Visible = false;
                lbl_noHoras.Text = "Para el rango indicado, no existen horas o el intervalo es incorrecto";
            }
            else {
                btnPDF.Visible = true;
                btnExcell.Visible = true;
                gd_reports.Visible = true;
                lbl_noHoras.Visible = false;
                gd_reports.DataSource = taskBusiness.getDatesByRange2(txt_desde.Text, txt_hasta.Text, Session["idCard_Report"].ToString());
                gd_reports.DataBind();
            }
        }//btnReport_Click

        //evento que se desencadena cuando se presiona el boton "Exportar PDF"
        protected void btnPDF_Click(object sender, EventArgs e)
        {
            if (txt_desde.Text=="" || txt_hasta.Text=="") {
                lbl_noHoras.Text = "Favor ingresar el rango de horas";
            }
            doPDF();
        }//btnPDF_Click

        protected void doPDF() {
            //se obtiene el nombre del colaborador, esto para el nombre del PDF
            string nombre = collaboratorBusiness.GetNameColaboratorById(Session["idCard_Report"].ToString());
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = nombre + numero;

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename="+salida+".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gd_reports.AllowPaging = false;
            gd_reports.DataSource = taskBusiness.getDatesByRange2(txt_desde.Text, txt_hasta.Text, Session["idCard_Report"].ToString());
            gd_reports.DataBind();
            gd_reports.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());

            Document pdfDoc = new Document(PageSize.A4,10f,10f,10f,0f);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            PdfWriter.GetInstance(pdfDoc,Response.OutputStream);

            pdfDoc.Open();

            //agregando una imagen
            //iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("C:/Users/Gustavo/Documents/GitHub/CoreVises/AppNode2017/img/tree.png");
            //imagen.BorderWidth = 0;
            //imagen.Alignment = Element.ALIGN_TOP;
            //imagen.Alignment = Element.ALIGN_LEFT;
            //float percentage = 0.0f;
            //percentage = 80 / imagen.Width;
            //imagen.ScalePercent(percentage * 100);

            //insertamos la imagen
            //pdfDoc.Add(imagen);

            //encabezado
            pdfDoc.Add(new Paragraph("---------------------------------------------------------------------------------------------------------------------------"));
            string date = "Fecha del reporte:" + DateTime.Now.ToString();
            Chunk chunk = new Chunk("Reporte Sobre horas laboradas", FontFactory.GetFont("ARIAL", 16, iTextSharp.text.Font.BOLD));
            pdfDoc.Add(new Paragraph(chunk));
            pdfDoc.Add(new Paragraph("Colaborador: "+ collaboratorBusiness.CollaboratorFullName(Session["idCard_Report"].ToString())));
            pdfDoc.Add(new Paragraph("Periodo: "+txt_desde.Text+" --- "+txt_hasta.Text));
            pdfDoc.Add(new Paragraph("XXX, Costa Rica"));
            pdfDoc.Add(new Paragraph(date));
            pdfDoc.Add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------"));
            pdfDoc.Add(new Paragraph("                       "));

            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }//doPDF
        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        //boton cancelar que esta junto al boton de generar PDF
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            gd_colaboradores.Visible = true;
            gd_reports.Visible = false;

            lbl_noHoras.Text = "";

            txt_desde.Text = "";
            txt_hasta.Text = "";

            lbl_parra.Text="";
        }//btnCancelar_Click

        //boton cancelar que esta junto al boton de generar reporte
        protected void btnCancelReport_Click(object sender, EventArgs e)
        {
            gd_colaboradores.Visible = true;
            gd_reports.Visible = false;

            lbl_noHoras.Text = "";

            txt_desde.Text = "";
            txt_hasta.Text = "";

            lbl_parra.Text="";
        }//btnCancelReport_Click

        //evento que se activa al darle click al boton de generar un excell
        protected void btnExcell_Click(object sender, EventArgs e)
        {
            exportToExcell(sender,e);
        }//btnExcell_Click

        //metodo para crear el excell del reporte
        protected void exportToExcell(object sender, EventArgs e) {
            //se obtiene el nombre del colaborador, esto para el nombre del PDF
            string nombre = collaboratorBusiness.GetNameColaboratorById(Session["idCard_Report"].ToString());
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = nombre + numero;

            //inicia el flujo de excell
            Response.Clear();
            Response.Buffer = true;
            //se agrega el nombre del colaborador a evaluar  al nombre del excell
            Response.AddHeader("content-disposition", "attachment;filename="+salida+".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter textWritter = new HtmlTextWriter(sw);

                //se agrega el gridview
                gd_reports.AllowPaging = false;
                gd_reports.DataSource = taskBusiness.getDatesByRange2(txt_desde.Text, txt_hasta.Text, Session["idCard_Report"].ToString());
                gd_reports.DataBind();

                //se le pone color a los encabezados
                gd_reports.HeaderRow.BackColor = Color.White;

                //se obtienen las filas del gridview
                foreach (GridViewRow row in gd_reports.Rows)
                {
                    row.BackColor = Color.White;
                    //se recorren las celdad
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gd_reports.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gd_reports.RowStyle.BackColor;
                        }
                    }//foreach
                }//foreach
                gd_reports.RenderControl(textWritter);

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }//using
        }//exportToExcell

        protected void exportToExcelReport1(object sender, EventArgs e)
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Horas por Proyecto. ID: " + numero;

            //inicia el flujo de excell
            Response.Clear();
            Response.Buffer = true;
            //se agrega el nombre del colaborador a evaluar  al nombre del excell
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter textWritter = new HtmlTextWriter(sw);

                //se agrega el gridview
                gd_report1.Visible = true;

                gd_report1.AllowPaging = false;
                gd_report1.DataSource = taskBusiness.reporteUno();
                gd_report1.DataBind();

                //se le pone color a los encabezados
                gd_report1.HeaderRow.BackColor = Color.White;

                //se obtienen las filas del gridview
                foreach (GridViewRow row in gd_report1.Rows)
                {
                    row.BackColor = Color.White;
                    //se recorren las celdad
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gd_report1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gd_report1.RowStyle.BackColor;
                        }
                    }//foreach
                }//foreach
                gd_report1.RenderControl(textWritter);

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }//using
        }//exportToExcell

        protected void exportToExcelReport2(object sender, EventArgs e)
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Horas totales por Proyecto. ID: " + numero;

            //inicia el flujo de excell
            Response.Clear();
            Response.Buffer = true;
            //se agrega el nombre del colaborador a evaluar  al nombre del excell
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter textWritter = new HtmlTextWriter(sw);

                //se agrega el gridview
                gd_report2.Visible = true;

                gd_report2.AllowPaging = false;
                gd_report2.DataSource = taskBusiness.reporteDos();
                gd_report2.DataBind();

                //se le pone color a los encabezados
                gd_report2.HeaderRow.BackColor = Color.White;

                //se obtienen las filas del gridview
                foreach (GridViewRow row in gd_report2.Rows)
                {
                    row.BackColor = Color.White;
                    //se recorren las celdad
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gd_report2.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gd_report2.RowStyle.BackColor;
                        }
                    }//foreach
                }//foreach
                gd_report2.RenderControl(textWritter);

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }//using
        }//exportToExcell

        protected void exportToExcelReport3(object sender, EventArgs e)
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Reporte General. ID: " + numero;

            //inicia el flujo de excell
            Response.Clear();
            Response.Buffer = true;
            //se agrega el nombre del colaborador a evaluar  al nombre del excell
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter textWritter = new HtmlTextWriter(sw);

                //se agrega el gridview
                gd_report3.Visible = true;

                gd_report3.AllowPaging = false;
                gd_report3.DataSource = taskBusiness.reporteTres();
                gd_report3.DataBind();

                //se le pone color a los encabezados
                gd_report3.HeaderRow.BackColor = Color.White;

                //se obtienen las filas del gridview
                foreach (GridViewRow row in gd_report3.Rows)
                {
                    row.BackColor = Color.White;
                    //se recorren las celdad
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gd_report3.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gd_report3.RowStyle.BackColor;
                        }
                    }//foreach
                }//foreach
                gd_report3.RenderControl(textWritter);

                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }//using
        }//exportToExcell

        protected void doPDF_Report1()
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Horas por Proyecto. ID: " + numero;

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gd_report1.Visible = true;

            gd_report1.AllowPaging = false;
            gd_report1.DataSource = taskBusiness.reporteUno();
            gd_report1.DataBind();
            gd_report1.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();

            //encabezado
            pdfDoc.Add(new Paragraph("---------------------------------------------------------------------------------------------------------------------------"));
            string date = "Fecha del reporte:" + DateTime.Now.ToString();
            Chunk chunk = new Chunk("Reporte sobre horas totales laboradas por proyecto", FontFactory.GetFont("ARIAL", 16, iTextSharp.text.Font.BOLD));
            pdfDoc.Add(new Paragraph(chunk));
            pdfDoc.Add(new Paragraph("Hecho por: " + collaboratorBusiness.GetNameColaborator((string)Session["user"])));
            pdfDoc.Add(new Paragraph("XXX, Costa Rica"));
            pdfDoc.Add(new Paragraph(date));
            pdfDoc.Add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------"));
            pdfDoc.Add(new Paragraph("                       "));

            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }//doPDF

        protected void doPDF_Report2()
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Horas_Colaborador_Por_Proyecto. ID: " + numero;

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gd_report2.Visible = true;

            gd_report2.AllowPaging = false;
            gd_report2.DataSource = taskBusiness.reporteDos();
            gd_report2.DataBind();
            gd_report2.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();

            //encabezado
            pdfDoc.Add(new Paragraph("---------------------------------------------------------------------------------------------------------------------------"));
            string date = "Fecha del reporte:" + DateTime.Now.ToString();
            Chunk chunk = new Chunk("Reporte sobre el total de las horas trabajadas por los colaboradores en los diferentes proyectos", FontFactory.GetFont("ARIAL", 16, iTextSharp.text.Font.BOLD));
            pdfDoc.Add(new Paragraph(chunk));
            pdfDoc.Add(new Paragraph("Hecho por: " + collaboratorBusiness.GetNameColaborator((string)Session["user"])));
            pdfDoc.Add(new Paragraph("XXX, Costa Rica"));
            pdfDoc.Add(new Paragraph(date));
            pdfDoc.Add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------"));
            pdfDoc.Add(new Paragraph("                       "));

            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }//doPDF

        protected void doPDF_Report3()
        {
            //se crea un numero aleatorio entre 1000-9999 para el PDF
            Random random = new Random();
            string numero = random.Next(100000, 999999).ToString();
            //se une el nombre del colaborador y el numero aleatorio
            string salida = "Reporte General. ID: " + numero;

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + salida + ".pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            gd_report3.Visible = true;

            gd_report3.AllowPaging = false;
            gd_report3.DataSource = taskBusiness.reporteTres();
            gd_report3.DataBind();
            gd_report3.RenderControl(hw);

            StringReader sr = new StringReader(sw.ToString());

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

            pdfDoc.Open();

            //encabezado
            pdfDoc.Add(new Paragraph("---------------------------------------------------------------------------------------------------------------------------"));
            string date = "Fecha del reporte: " + DateTime.Now.ToString();
            Chunk chunk = new Chunk("Reporte general total de horas laboradas", FontFactory.GetFont("ARIAL", 16, iTextSharp.text.Font.BOLD));
            pdfDoc.Add(new Paragraph(chunk));
            pdfDoc.Add(new Paragraph("Hecho por: " + collaboratorBusiness.GetNameColaborator((string)Session["user"])));
            pdfDoc.Add(new Paragraph("XXX, Costa Rica"));
            pdfDoc.Add(new Paragraph(date));
            pdfDoc.Add(new Paragraph("----------------------------------------------------------------------------------------------------------------------------"));
            pdfDoc.Add(new Paragraph("                       "));

            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }//doPDF

        protected void btnRep_Click(object sender, EventArgs e)
        {
            if (ddl_reportes.SelectedValue == "HorasXProyecto")
            {
                doPDF_Report1();
            }
            else if (ddl_reportes.SelectedValue == "MasHorasCol")
            {
                doPDF_Report2();
            } else if (ddl_reportes.SelectedValue == "RepGeneral")
            {
                doPDF_Report3();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (ddl_reportes.SelectedValue == "HorasXProyecto")
            {
                exportToExcelReport1(sender, e);
            }
            else if (ddl_reportes.SelectedValue == "MasHorasCol")
            {
                exportToExcelReport2(sender,e);
            }
            else if (ddl_reportes.SelectedValue == "RepGeneral")
            {
                exportToExcelReport3(sender,e);
            }
        }
    }//CLASE
}//namespace