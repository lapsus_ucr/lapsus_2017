﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace Interfaz
{
    public partial class MyTasks : System.Web.UI.Page
    {
        string connectionString;
        private TaskBusiness taskBusiness;
        private CategoryBusiness categoryBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        private ProyectBusiness projectBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;
        private ExtraHoursBusiness extraHoursBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            categoryBusiness = new CategoryBusiness(connectionString);
            projectBusiness = new ProyectBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);
            extraHoursBusiness = new ExtraHoursBusiness(connectionString);
            taskBusiness = new TaskBusiness(connectionString);

            //Nombre de usuario en la barra de Menu
            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);

            //Cuando la pagina carga no se muestra el formulario para Registrar Horas
            formNewTask.Visible = false;
            btnRegisterExtraHours.Visible = false;

            //Se obtienen los IDs de Categoria y Proyecto
            categoryBusiness.GetCategoriesIds();
            projectBusiness.GetProyectsIds();

            //Se crean listas con los nombres de todas las categorias y
            //con los nombres de todos los proyectos.
            //Con el fin e llenar los DropDown respectivos
            LinkedList<Category> cat = categoryBusiness.GetCategoriesNames();
            LinkedList<Proyect> proy = projectBusiness.GetAllProjectNames();

            //Lo que esta dentro de este IF solo se carga UNA vez
            if (!IsPostBack)
            {
                //si la persona no tiene tareas registradas
                //se imprime en interfaz
                if (getData() == false)
                {
                    lbl_title.Text = "Aún no tenés tareas registradas";
                    lbl_parra.Text = "";
                    divFiltrado.Visible = false;
                }
                else
                {
                    divFiltrado.Visible = true;
                    taskBusiness.getAllIDTaskByIdCard((string)Session["user"]);
                }

                //se llena el DropDownList de Categoria con los nombres que estan en
                //la BD
                foreach (Category category in cat)
                {
                    ddl_Categoria.Items.Add(category.Name);
                }

                //se le agrega Un item meramente visual al DropDown
                //NO SE PUEDE SELECCIONAR
                ddl_Categoria.Items.Insert(0, new ListItem("Categoría", ""));

                //se llena el DropDownList de Proyecto con los nombres que estan en
                //la BD
                foreach (Proyect project in proy)
                {
                    ddl_Proyecto.Items.Add(project.Name);
                }

                //se le agrega Un item meramente visual al DropDown
                //NO SE PUEDE SELECCIONAR
                ddl_Proyecto.Items.Insert(0, new ListItem("Proyecto", ""));

                //se le agrega Un item meramente visual al DropDown
                //NO SE PUEDE SELECCIONAR
                ddl_horas.Items.Insert(0, new ListItem("Horas Regulares", "0"));

                //se le agrega Un item meramente visual al DropDown
                //NO SE PUEDE SELECCIONAR
                ddl_horasExtra.Items.Insert(0, new ListItem("Horas Extra", "0"));
            }

        }//Page_Load

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

        //metodo encargado de cargar el gridView
        //si no hay registros de tareas para esa persona, el metodo retorna false
        //esto con el fin de cambiar la información de la página al cargar
        private bool getData()
        {
            collaboratorBusiness = new ColaboratorBusiness(connectionString);

            DataSet dataset = taskBusiness.getAllTaskByIdCard(collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));

            //si el dataset esta vacio, o sea, la persona no tiene tareas registradas
            if (dataset.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                gd_tareas.DataSource = dataset;
                gd_tareas.DataBind();
                return true;
            }
        }//getData

        //metodo encargado de obtener los datos del campo FECHA cada vez que 
        //se recarga la pagina
        private void getFirstDate(GridViewEditEventArgs e) {
            string firstDate = gd_tareas.Rows[e.NewEditIndex].Cells[5].Text;
        }//getFirstDate

        protected void gd_tareas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gd_tareas.EditIndex = -1;

            getData();

        }//gd_tareas_RowCancelingEdit

        protected void gd_tareas_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gd_tareas.EditIndex = e.NewEditIndex;

            getData();
        }//gd_tareas_RowEditing

        protected void gd_tareas_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Session["idTask_Grid"] = gd_tareas.DataKeys[e.RowIndex].Values["idTask"].ToString();
            Response.Redirect("ConfirmDelete.aspx");

        }//gd_tareas_RowDeleting

        //Cuando se presiona el boton Update del GridView, se activa este evento
        protected void gd_tareas_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //Se crean los DropDownList que van a contener los DropDown del GridView
            DropDownList ddl_new = new DropDownList();
            DropDownList ddl_horas_extra = new DropDownList();
            DropDownList txtHoras = new DropDownList();
            DropDownList txtIdCategoria = new DropDownList();

            //Se asignan los componentes del GridView a nuevos elementos
            //Se asignan mediante su ID (variable anaranjada)
            System.Web.UI.WebControls.TextBox txtIdTarea = gd_tareas.Rows[e.RowIndex].FindControl("txt_idTarea") as System.Web.UI.WebControls.TextBox;
            txtIdCategoria = gd_tareas.Rows[e.RowIndex].FindControl("txt_idCategoria") as DropDownList;
            ddl_new = gd_tareas.Rows[e.RowIndex].FindControl("ddl_project") as DropDownList;
            System.Web.UI.WebControls.TextBox txtDescripcion = gd_tareas.Rows[e.RowIndex].FindControl("txt_descripcion") as System.Web.UI.WebControls.TextBox;
            System.Web.UI.WebControls.TextBox txtFecha = gd_tareas.Rows[e.RowIndex].FindControl("txt_Fecha") as System.Web.UI.WebControls.TextBox;
            txtHoras = gd_tareas.Rows[e.RowIndex].FindControl("ddl_horas") as DropDownList;
            ddl_horas_extra = gd_tareas.Rows[e.RowIndex].FindControl("ddl_horas_extra") as DropDownList;

            //Se instancian las clases para usar sus atributos
            Tasks task = new Tasks();
            Audit audit = new Audit();

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //A cada atributo se le asigna lo que el usuario ingreso
            //en el GridView
            task.IdTask = txtIdTarea.Text;
            task.IdCategory = txtIdCategoria.Text;
            task.IdProyect = ddl_new.Text;
            task.Description = txtDescripcion.Text;
            task.Hours = txtHoras.Text;
            task.TaskExtraHours = ddl_horas_extra.Text;
            task.IdCardColaborator = collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]);
            task.AuditUser= (string)Session["user"];
            task.AuditDate= DateTime.Today.ToString();
            task.AuditAction = "Editar";

            //se edita la tarea mediante el metodo EditTask del Business
            taskBusiness.EditTask(task, txtIdTarea.Text);

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Editar";
            audit.ActionTable = "Tasks";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            gd_tareas.EditIndex = -1;
            getData();

        }//gd_tareas_RowUpdating

        //Metodo con el cual se activa el evento el boton 
        //"Registrar Horas"
        protected void btnRegisterHour_Click(object sender, EventArgs e)
        {
            formNewTask.Visible = true;
            btnRegisterHours.Visible = false;
            btnRegisterExtraHours.Visible = false;
        }

        protected void btnRegisterExtraHour_Click(object sender, EventArgs e)
        {

        }

        protected void btn_Registrar_Click(object sender, EventArgs e)
        {
            //se obtienen los datos del formulario y se colocan en variables
            string nameCategoria = ddl_Categoria.SelectedItem.Text;
            string nameProyecto = ddl_Proyecto.SelectedItem.Text;
            string descripcion = txt_Descripcion.Text;
            string fecha = DateTime.Today.ToString();
            string horas = ddl_horas.SelectedItem.Value;
            string horasExtra = ddl_horasExtra.SelectedItem.Value;
            string cedula = collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]);

            //se parsea el string a double
            //se debe deinir la Cultura, pues al parsear de string a double, se pierde la coma
            //o punto y queda como un numero entero
            double horasRegulares = Convert.ToDouble(horas.ToString(), CultureInfo.InvariantCulture);
            double horasExtraConver = Convert.ToDouble(horasExtra.ToString(), CultureInfo.InvariantCulture);

            if (horas == "0" && horasExtra == "0")
            {
                lblMensaje.Text = "Se debe registrar al menos un tipo de hora";
            }
            else if (horasExtraConver>(24-horasRegulares))
            {
                lblMensaje.Text = "No se pueden ingresar más de 24 horas";
            }
            else
            {
                //se instancia el Business
                TaskBusiness tBusiness = new TaskBusiness(connectionString);

                //se instancia el Domain
                Tasks task = new Tasks();
                Audit audit = new Audit();
                ExtraHours extraHours = new ExtraHours();

                //se crea una variables de tipo Random
                //para su posterior uso
                Random random = new Random();


                //se llena el objeto Tasks con los datos a insertar
                task.IdTask = random.Next(100000, 999999).ToString();
                task.IdCategory = nameCategoria;
                task.IdProyect = nameProyecto;
                task.Description = descripcion;
                task.Date = fecha;
                task.Hours = horas;
                task.TaskExtraHours = horasExtra;
                task.IdCardColaborator = cedula;
                task.AuditUser = (string)Session["user"];
                task.AuditDate = DateTime.Today.ToString();
                task.AuditAction = "Insertar";

                //se llama al Business y se inserta la tarea nueva
                tBusiness.InsertTask(task);

                //se llena el objeto Audit con los datos a insertar
                audit.ActionId = random.Next(100000, 999999);
                audit.ActionDate = DateTime.Today.ToString();
                audit.ActionMade = "Insertar";
                audit.ActionTable = "Tasks";
                audit.ActionUser = (string)Session["user"];
                audit.ActionUserRole = logInBusiness.GetRole(
                    collaboratorBusiness.GetIdCardColaborator(
                        (string)Session["user"]));

                //se llama al Business y se inserta la nueva Auditoria
                auditBusiness.InsertAudit(audit);

                //se vuelve a la misma pagina para ver los cambios efectuados
                Response.Redirect("MyTasks.aspx");
            }
            
        }//btn_Registrar_Click

        //Metodo para el boton Cancelar del Formulario de Registro
        //Se esconde el formulario y el boton de Horas Extra
        //Se hace visible el boton de Registrar Horas
        protected void btn_Cancelar_Click(object sender, EventArgs e)
        {
            formNewTask.Visible = false;
            btnRegisterHours.Visible = true;
            btnRegisterExtraHours.Visible = false;
        }

        //Con este metodo se carga el dropdowlist que contiene el nombre de los 
        //proyectos.
        //Esto a la hora que se pulsa sobre el boton "Edit" del GridVview
        protected void gd_tareas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //obtengo el ID de la tarea a editar. Esto con el fin de utilizarlo mas
            //adelante para obtener el nombre de su proyecto respectivo
            System.Web.UI.WebControls.TextBox idTask = (System.Web.UI.WebControls.TextBox)e.Row.FindControl("txt_idTarea");

            if (e.Row.RowType == DataControlRowType.DataRow && gd_tareas.EditIndex == e.Row.RowIndex)
            {
                DropDownList projects = (DropDownList)e.Row.FindControl("ddl_project");
                DropDownList categories = (DropDownList)e.Row.FindControl("txt_idCategoria");

                LinkedList<Proyect> proy = projectBusiness.GetAllProjectNames();
                LinkedList<Category> cat = categoryBusiness.GetCategoriesNames();

                foreach (Proyect project in proy)
                {
                    projects.Items.Add(project.Name);
                }//foreach

                foreach (Category category in cat)
                {
                    categories.Items.Add(category.Name);
                }//foreach

                //con esta linea hago que el DropDownList muestre el valor que yo quiero de 
                //primero. Y no el que esta de primero en la fila.
                projects.SelectedValue = taskBusiness.getTaskNameById(idTask.Text);
                categories.SelectedValue = taskBusiness.getCategoryNameByTaskId(idTask.Text);
            }//if
        }//gd_tareas_RowDataBound

        //Evento que se activa en el momento que se presiona sobre el link "Duplicar"
        protected void duplicate_link_Command(object sender, CommandEventArgs e)
        {
            //se pregunta si el comando es el indicado en el Link del GridView
            if (e.CommandName== "duplicate_field") {

                //se obtiene el ID de la fila a editar
                //esto se logra con el CommandoArgument declarado en el Link
                int index_row = Int32.Parse(e.CommandArgument.ToString());

                //se crea una Lista y se llena con la informacion de la tarea seleccionada
                //en el gridview, esto con el fin de duplicarla
                LinkedList<Tasks> tasks = taskBusiness.GetTaskForDuplicate(index_row.ToString());

                //Se instancian las clases para usar sus atributos
                Tasks task = new Tasks();
                Audit audit = new Audit();

                //se crea una variables de tipo Random
                //para su posterior uso
                Random random = new Random();

                //se recorre la lista de tareas y se asignan los valores al objeto ask
                foreach (Tasks myTask in tasks)
                {
                    task.IdCategory = myTask.IdCategory;
                    task.IdProyect = myTask.IdProyect;
                    task.Description = myTask.Description;
                    task.Date = myTask.Date;
                    task.Hours = myTask.Hours;
                    task.TaskExtraHours = myTask.TaskExtraHours;
                }

                //A cada atributo se le asigna lo que el usuario ingreso
                //en el GridView
                //se llena el objeto Tasks con los datos a insertar
                task.IdTask = random.Next(100000, 999999).ToString();
                task.IdCardColaborator = collaboratorBusiness.GetIdCardColaborator(
                                         (string)Session["user"]);
                task.AuditUser = (string)Session["user"];
                task.AuditDate = DateTime.Today.ToString();
                task.AuditAction = "Duplicar";

                taskBusiness.InsertTask(task);

                //se vuelve a la misma pagina para ver los cambios efectuados
                Response.Redirect("MyTasks.aspx");
            }
        }

        //evento que se activa cuando se pulsa el boton de buscar en el filtro
        protected void btn_buscar_Click(object sender, EventArgs e)
        {
            DataSet dataset = taskBusiness.getAllTaskByIdCard(collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));

            if (txtpyc.Text == "" && txtFechaInicio.Text == "" && txtFechaFin.Text == "")
            {
                if (dataset.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen horas registradas";
                    gd_tareas.Visible = false;
                }
                else {
                    gd_tareas.Visible = true;
                    gd_tareas.DataSource = dataset;
                    gd_tareas.DataBind();
                }
            }
            else if (txtpyc.Text != "" && txtFechaInicio.Text == "" && txtFechaFin.Text == "")
            {
                DataSet datasetLike = taskBusiness.GetTaskLike(txtpyc.Text,
                collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));
                if (datasetLike.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen registros con la categoría: "+ txtpyc.Text;
                    gd_tareas.Visible = false;
                }else {
                    gd_tareas.Visible = true;
                    gd_tareas.DataSource = datasetLike;
                    gd_tareas.DataBind();
                }
            } else if (txtpyc.Text == "" && txtFechaInicio.Text != "" && txtFechaFin.Text != "")
            {
                if (dataset.Tables[0].Rows.Count == 0)
                {
                    lbl_filter.Text = "No existen registros en el rango de fechas indicado";
                    gd_tareas.Visible = false;
                }
                else
                {
                    gd_tareas.Visible = true;
                    DataSet dataRange = taskBusiness.getDatesByRange2(txtFechaInicio.Text,txtFechaFin.Text, 
                        collaboratorBusiness.GetIdCardColaborator((string)Session["user"]));
                    gd_tareas.DataSource = dataRange;
                    gd_tareas.DataBind();
                    lbl_filter.Text ="";
                }
            }
        }//btn_buscar_Click
    }//CLASS
}//Namespace