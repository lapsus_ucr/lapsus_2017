﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain;
using Business;
using System.Web.Configuration;

namespace Interfaz
{
    public partial class ConfirmDelete : System.Web.UI.Page
    {
        string connectionString;
        private TaskBusiness taskBusiness;
        private AuditBusiness auditBusiness;
        private LogInBusiness logInBusiness;
        private ColaboratorBusiness collaboratorBusiness;

        protected void Page_Load(object sender, EventArgs e)
        {
            //se crea la conexion
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            
            //se instancias las clases Business necesarias
            taskBusiness = new TaskBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);
        }//Page_Load

        protected void btn_Yes_Click(object sender, EventArgs e)
        {
            //se instancia el Domain
            Audit audit = new Audit();

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Eliminar";
            audit.ActionTable = "Tasks";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            //se elimina la tarea mediante el ID elegido
            //este ID viene por medio de una variable de sesion
            //pues viaja por distintas vistas
            taskBusiness.RemoveTask((string)Session["idTask_Grid"]);

            //al efectuarse el borrado, se devuelve a la pagina de Tareas
            //esto con el fin de ver que se borro bien la tarea
            Response.Redirect("MyTasks.aspx");
        }//btn_Yes_Click

        protected void btn_No_Click(object sender, EventArgs e)
        {
            Response.Redirect("MyTasks.aspx");
        }//btn_No_Click
    }//class
}//namespace