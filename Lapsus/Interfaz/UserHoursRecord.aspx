﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserHoursRecord.aspx.cs" Inherits="Interfaz.UserHoursRecord" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shorcut icon" href="img/LogoXXX.ico" />
    <title>Manage Hours</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>
   
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="container">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="UserView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30" />
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                    </div>
                </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <br />
        <br />
        <form role="form" runat="server">
            <div  runat="server" id="formNewTask" class="container center"  style="margin-bottom: 15px; width:300px;">
                <h6 style="font-size: 2.5em; text-align: center; margin-top: 20px;">
                    Buscá Tus Horas
                </h6>
                <br/>
                <asp:Label id="labelDesde" AssociatedControlId="txt_Desde" Text="Desde" runat="server" />
                <asp:TextBox ID="txt_Desde" CssClass="form-control" type="date" runat="server" Font-Size="Large" placeholder="Desde" AutoCompleteType="Disabled"></asp:TextBox>
                 <br/>
                <asp:Label id="labelHasta" AssociatedControlId="txt_Hasta" Text="Hasta" runat="server" />
                <asp:TextBox ID="txt_Hasta" CssClass="form-control" type="date" runat="server" Font-Size="Large" placeholder="Hasta" AutoCompleteType="Disabled"></asp:TextBox>
                 <br />
                <div style="margin-top: 10px; text-align: center;">
                    <asp:Button ID="btn_Normales" runat="server" Text="Normales" CssClass="btn btn-danger btn-lg" OnClick="btn_Normales_Click"  Height="36px" />
                    <asp:Button ID="btn_Extra" runat="server" Text="Extra" CssClass="btn btn-danger btn-lg" OnClick="btn_Extra_Click"  Height="36px" />
                </div>     
            </div>

            <div  class="container">
                <%--<div  class="container center" style="margin-bottom: 50px">
                    <h1>
                        <asp:Label runat="server" ID="lbl_title">
                        Historial de Horas registradas
                        </asp:Label>
                    </h1>
                    <p>
                        <asp:Label runat="server" ID="lbl_parra">
                            Podés filtrar la búsqueda por un rango de fechas. Y decidir si querés
                            ver las horas normales o las horas extra.
                        </asp:Label>
                    </p>
                </div>--%>
                <br />              
                <div class="col-lg-12 " style="margin-top:80px;">
                    <div class="table-responsive">
                        <asp:GridView ID="gd_horas_normales" runat="server" Width="100%"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="false" DataKeyNames="idTask">
                            <Columns>
                                <asp:BoundField DataField="idTask" HeaderText="ID Tarea"/>
                                <asp:BoundField DataField="nameCategory" HeaderText="Categoría"/>
                                <asp:BoundField DataField="nameProject" HeaderText="Proyecto"/>
                                <asp:BoundField DataField="taskDescription" HeaderText="Descripción"/>
                                <asp:BoundField DataField="taskDate" HeaderText="Fecha"/>
                                <asp:BoundField DataField="taskHours" HeaderText="Horas"/>
                                <asp:BoundField DataField="idCardCollaborator" HeaderText="Cédula"/>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </div>
            <div class="container center" style="margin-bottom: 50px">
             
            </div>
        </form>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>
    </div>
</body>
</html>

