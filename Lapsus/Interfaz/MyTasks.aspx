﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTasks.aspx.cs" Inherits="Interfaz.MyTasks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
   <link rel="shorcut icon" href="img/LogoXXX.ico" />
    <title>Administrar Horas</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap-theme.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.css.map" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css.map" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap.css.map" rel="stylesheet" />
    <link href="css/CSS.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css.map" rel="stylesheet" />
    <script src="scripts/jquery-1.9.1.min.js"></script>
    <script src="scripts/bootstrap.js"></script>
    <script src="scripts/jquery-1.9.1.js"></script>
    <script src="scripts/jquery-1.9.1.intellisense.js"></script>
   
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    
 
</head>
<body>
    <div class="container">
        <div class="container">
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="UserView.aspx"><span>
                            <img alt="Logo" src="img/xxx.png" height="30" />
                            Lapsus </span></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <a class="navbar-brand navbar-right" href="LogOut.aspx">Salir</a>
                        <a class="navbar-brand navbar-right" >      </a>
                        <a class="navbar-brand navbar-right" href="#"><asp:Label ID="Label2" runat="server"  /></a>
                        
                    </div>
                </div>
            </div>
            <div class="container center" style="background-color:#E7E7E3; margin-top:15px" >
              <div style="margin-top:7px">
                <div class="row">               
                <ol class="breadcrumb" style="background-color:#E7E7E3;">
                <li><a href="UserView.aspx">Perfil Colaborador</a></li>
                <li class="active">Administrar Horas</li>
                </ol>
                </div>                
            </div>
            </div>
        </div>
        <!--Login Form --->
        <br />
        <form role="form" runat="server">
            <div  runat="server" id="formNewTask" class="container center"  style="margin-bottom: 15px; width:300px;">
            <h6 style="font-size: 2.5em; text-align: center; margin-top: 10px;">
                Registrá las horas de hoy
            </h6>
            <br/>
            <%--<label style="font-size:14px;">Categoría</label>--%>
            <asp:DropDownList ID="ddl_Categoria" runat="server" Font-Size="Large" CssClass="form-control"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="ddl_requerido_categoria" runat="server" 
                                        ControlToValidate="ddl_Categoria"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <%--<label style="font-size:20px; margin-top:10px;"">Proyecto</label>--%>
            <asp:DropDownList ID="ddl_Proyecto" runat="server" Font-Size="Large" CssClass="form-control"></asp:DropDownList>
            <asp:RequiredFieldValidator ID="ddl_requerido_proyecto" runat="server" 
                                        ControlToValidate="ddl_Proyecto"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <%--<label style="font-size:20px; margin-top:10px;"">Descripción</label>--%>
            <asp:TextBox ID="txt_Descripcion" CssClass="form-control" type="text" runat="server" Font-Size="Large" placeholder="Descripción" AutoCompleteType="Disabled" ></asp:TextBox>
            <asp:RequiredFieldValidator ID="txt_requerido_proyecto" runat="server" 
                                        ControlToValidate="txt_Descripcion"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>
            <br />
            <%--<label style="font-size:20px; margin-top:10px;"">Horas trabajadas</label>--%>
            <asp:DropDownList ID="ddl_horas" runat="server" Font-Size="Large" CssClass="form-control">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="1.5" Text="1.5"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="2.5" Text="2.5"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="3.5" Text="3.5"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="4.5" Text="4.5"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="5.5" Text="5.5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                <asp:ListItem Value="6.5" Text="6.5"></asp:ListItem>
                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                <asp:ListItem Value="7.5" Text="7.5"></asp:ListItem>
                <asp:ListItem Value="8" Text="8"></asp:ListItem>
            </asp:DropDownList>
            <%--<asp:RequiredFieldValidator ID="ddl_horas_requerido" runat="server" 
                                        ControlToValidate="ddl_horas"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>--%>
            <br />
            <asp:DropDownList ID="ddl_horasExtra" runat="server" Font-Size="Large" CssClass="form-control">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="1.5" Text="1.5"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="2.5" Text="2.5"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="3.5" Text="3.5"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="4.5" Text="4.5"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="5.5" Text="5.5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                <asp:ListItem Value="6.5" Text="6.5"></asp:ListItem>
                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                <asp:ListItem Value="7.5" Text="7.5"></asp:ListItem>
                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                <asp:ListItem Value="8.5" Text="8.5"></asp:ListItem>
                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                <asp:ListItem Value="9.5" Text="9.5"></asp:ListItem>
                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                <asp:ListItem Value="10.5" Text="10.5"></asp:ListItem>
                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                <asp:ListItem Value="11.5" Text="11.5"></asp:ListItem>
                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                <asp:ListItem Value="12.5" Text="12.5"></asp:ListItem>
                <asp:ListItem Value="13" Text="13"></asp:ListItem>
                <asp:ListItem Value="13.5" Text="13.5"></asp:ListItem>
                <asp:ListItem Value="14" Text="14"></asp:ListItem>
                <asp:ListItem Value="14.5" Text="14.5"></asp:ListItem>
                <asp:ListItem Value="15" Text="15"></asp:ListItem>
                <asp:ListItem Value="15.5" Text="15.5"></asp:ListItem>
                <asp:ListItem Value="16" Text="16"></asp:ListItem>
                <asp:ListItem Value="16.5" Text="16.5"></asp:ListItem>
                <asp:ListItem Value="17" Text="17"></asp:ListItem>
                <asp:ListItem Value="17.5" Text="17.5"></asp:ListItem>
                <asp:ListItem Value="18" Text="18"></asp:ListItem>
                <asp:ListItem Value="18.5" Text="18.5"></asp:ListItem>
                <asp:ListItem Value="19" Text="19"></asp:ListItem>
                <asp:ListItem Value="19.5" Text="19.5"></asp:ListItem>
                <asp:ListItem Value="20" Text="20"></asp:ListItem>
                <asp:ListItem Value="20.5" Text="20.5"></asp:ListItem>
                <asp:ListItem Value="21" Text="21"></asp:ListItem>
                <asp:ListItem Value="21.5" Text="21.5"></asp:ListItem>
                <asp:ListItem Value="22" Text="22"></asp:ListItem>
                <asp:ListItem Value="22.5" Text="22.5"></asp:ListItem>
                <asp:ListItem Value="23" Text="23"></asp:ListItem>
                <asp:ListItem Value="23.5" Text="23.5"></asp:ListItem>
                <asp:ListItem Value="24" Text="24"></asp:ListItem>
            </asp:DropDownList>
            <%--<asp:RequiredFieldValidator ID="ddl_horasExtra_requerido" runat="server" 
                                        ControlToValidate="ddl_horasExtra"
                                        ErrorMessage="Campo requerido"
                                        ForeColor="Red">
            </asp:RequiredFieldValidator>--%>

            <asp:ScriptManager ID="script" runat="server" EnablePartialRendering="true">
            </asp:ScriptManager>

            <div style="margin-top: 10px; text-align: center;">
                <asp:Button ID="btn_Registrar" runat="server" Text="Registrar" CssClass="btn btn-default" OnClick="btn_Registrar_Click" Height="36px" />
                <asp:Button ID="btn_Cancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger" OnClick="btn_Cancelar_Click" Height="36px" CausesValidation="false"/>
            </div>

            <asp:UpdatePanel ID="panel" runat="server"  UpdateMode="Conditional">
                <ContentTemplate>
                    <fieldset>
                         <div style="margin-top:20px;">
                            <asp:Label ID="lblMensaje" runat="server" CssClass="labelMensajes"></asp:Label>
                        </div>
                    </fieldset>
                </ContentTemplate>

                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btn_Registrar" EventName="Click">
                    </asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
                                     
            </div>
            <div  class="container">
                <div  class="container center" style="margin-bottom: 50px">
                    <h1>
                        <asp:Label runat="server" ID="lbl_title">
                        Panel de horas
                        </asp:Label>
                    </h1>
                    <p>
                        <asp:Label runat="server" ID="lbl_parra">
                        
                        </asp:Label>
                    </p>
                </div>
                <br />                 
                <div  runat="server" id="divFiltrado" class="col-lg-12"  style="margin-bottom: 15px; width:100%; height:auto;">
                    <h3>
                      <asp:Label runat="server"  ID="lbEncabezado">Filtrar por:</asp:Label>   
                    </h3>
                    <div class="left" >
                         <asp:Label runat="server" ID="lbFiltrado">Nombre:</asp:Label>    
                                      
                         <asp:TextBox ID="txtpyc"  type="text" Width="200px"  runat="server"  Font-Size="Small"  placeholder="Categoría" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                    <div class="right" style="margin-left:20px;">
                        <asp:Label runat="server" ID="lbDate">Rango de fechas:</asp:Label>
                       
                         <asp:TextBox ID="txtFechaInicio"  type="date" Width="170px"   runat="server" Font-Size="Small"  placeholder="Desde" AutoCompleteType="Disabled" ></asp:TextBox>
                         <asp:TextBox ID="txtFechaFin"  type="date" Width="170px"  runat="server" Font-Size="Small"  placeholder="Hasta" AutoCompleteType="Disabled" ></asp:TextBox>
                         <asp:Button runat="server" ID="btn_buscar" Text="Buscar" CssClass="btn btn-default btn-xs"  OnClick="btn_buscar_Click"/>
                    </div>
                </div>
                <div  class="container">
                    <div  class="container center">
                        <h3>
                            <asp:Label runat="server"  ID="lbl_filter"></asp:Label>   
                        </h3>
                    </div>
                </div>
                <br />             
                <div class="col-lg-12 ">
                    <div class="table-responsive">
                        <asp:GridView ID="gd_tareas" runat="server" Width="100%"
                            CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="false" DataKeyNames="idTask" 
                            AutoGenerateDeleteButton="true" AutoGenerateEditButton="true"
                            OnRowCancelingEdit="gd_tareas_RowCancelingEdit" OnRowEditing="gd_tareas_RowEditing"
                            OnRowDeleting="gd_tareas_RowDeleting" OnRowUpdating="gd_tareas_RowUpdating" 
                            OnRowDataBound="gd_tareas_RowDataBound" >
                           
                            <HeaderStyle BackColor="#666666" Font-Bold="True" ForeColor="White"/>
                          
                            <Columns>
                                <asp:TemplateField  HeaderText="Acción">
                                    <ItemTemplate >
                                        <%--<asp:Button runat="server" Text="Duplicar"/>--%>
                                        <%--<asp:HyperLink runat="server" Text="Duplicar" NavigateUrl="#"></asp:HyperLink>--%>
                                        <asp:LinkButton ID="duplicate_link" runat="server" Text="Duplicar" CommandArgument='<%#Eval("idTask")%>' CommandName="duplicate_field" OnCommand="duplicate_link_Command"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Tarea">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_idTarea" runat="server"
                                            Text='<%# Bind("idTask")%>' type="number" ReadOnly="true">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_idTarea" runat="server"
                                            Text='<%# Bind("idTask")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Categoría">
                                    <EditItemTemplate>
                                        <%--<asp:TextBox ID="txt_idCategoria" runat="server"
                                            Text='<%# Bind("nameCategory")%>'>
                                        </asp:TextBox>--%>
                                        <asp:DropDownList ID="txt_idCategoria" runat="server"
                                                          Font-Size="Small" CssClass="btn btn-inverse">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_idCategoria" runat="server"
                                            Text='<%# Bind("nameCategory")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Proyecto">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddl_project" runat="server"
                                                          Font-Size="Small" CssClass="btn btn-inverse">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_idProyecto" runat="server"
                                            Text='<%# Bind("nameProject")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Descripción">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_descripcion" runat="server"
                                            Text='<%# Bind("taskDescription")%>'>
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_descripcion" runat="server"
                                            Text='<%# Bind("taskDescription")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Fecha">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txt_Fecha" runat="server"
                                            Text='<%# Bind("taskDate")%>' ReadOnly="true">
                                        </asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Fecha" runat="server"
                                            Text='<%# Bind("taskDate")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Horas regulares">
                                    <EditItemTemplate>
                                        <%-- <asp:TextBox ID="txt_Horas" runat="server"
                                                 Text='<%# Bind("taskHours")%>'>
                                    </asp:TextBox>--%>
                                        <asp:DropDownList Text='<%# Bind("taskRegularHours")%>' ID="ddl_horas" runat="server" Font-Size="Small" CssClass="btn btn-inverse">
                                            <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1.5" Text="1.5"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                            <asp:ListItem Value="2.5" Text="2.5"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                            <asp:ListItem Value="3.5" Text="3.5"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                            <asp:ListItem Value="4.5" Text="4.5"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                            <asp:ListItem Value="5.5" Text="5.5"></asp:ListItem>
                                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                            <asp:ListItem Value="6.5" Text="6.5"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                            <asp:ListItem Value="7.5" Text="7.5"></asp:ListItem>
                                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_Horas" runat="server"
                                            Text='<%# Bind("taskRegularHours")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Horas Extra">
                                    <EditItemTemplate>
                                        <asp:DropDownList Text='<%# Bind("taskExtraHours")%>' ID="ddl_horas_extra" runat="server" Font-Size="Small" CssClass="btn btn-inverse">
                                                 <asp:ListItem Value="0" Text="0"></asp:ListItem>
                                                <asp:ListItem Value="1.5" Text="1.5"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="2.5" Text="2.5"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                <asp:ListItem Value="3.5" Text="3.5"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                <asp:ListItem Value="4.5" Text="4.5"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                <asp:ListItem Value="5.5" Text="5.5"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                <asp:ListItem Value="6.5" Text="6.5"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                <asp:ListItem Value="7.5" Text="7.5"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                <asp:ListItem Value="8.5" Text="8.5"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                <asp:ListItem Value="9.5" Text="9.5"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                <asp:ListItem Value="10.5" Text="10.5"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                <asp:ListItem Value="11.5" Text="11.5"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                <asp:ListItem Value="12.5" Text="12.5"></asp:ListItem>
                                                <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                                <asp:ListItem Value="13.5" Text="13.5"></asp:ListItem>
                                                <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                                <asp:ListItem Value="14.5" Text="14.5"></asp:ListItem>
                                                <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                                <asp:ListItem Value="15.5" Text="15.5"></asp:ListItem>
                                                <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                                <asp:ListItem Value="16.5" Text="16.5"></asp:ListItem>
                                                <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                                <asp:ListItem Value="17.5" Text="17.5"></asp:ListItem>
                                                <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                                <asp:ListItem Value="18.5" Text="18.5"></asp:ListItem>
                                                <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                                <asp:ListItem Value="19.5" Text="19.5"></asp:ListItem>
                                                <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                                <asp:ListItem Value="20.5" Text="20.5"></asp:ListItem>
                                                <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                                <asp:ListItem Value="21.5" Text="21.5"></asp:ListItem>
                                                <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                                <asp:ListItem Value="22.5" Text="22.5"></asp:ListItem>
                                                <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                                <asp:ListItem Value="23.5" Text="23.5"></asp:ListItem>
                                                <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                            </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_horas_extra" runat="server"
                                            Text='<%# Bind("taskExtraHours")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </div>
            <div class="container center" style="margin-bottom: 50px">
             <div class="form-group">
                              
                     <asp:Button ID="btnRegisterHours" runat="server" Text="Registrar Horas" CssClass="btn btn-danger" Height="36px" OnClick="btnRegisterHour_Click" />

                     <asp:Button ID="btnRegisterExtraHours" runat="server" Text="Registrar Horas Extra" CssClass="btn btn-danger" Height="36px" OnClick="btnRegisterExtraHour_Click" />
                     
                     
                </div>
           </div>
        </form>
        <footer>
            <a href="https://www.facebook.com/esteban.sanabriamora.5" target="_blank" title="XXX en Facebook">
                <img src="img/logo-facebook-20-20.gif" alt="Facebook" width="20" height="20" /></a>
            <a href="https://www.instagram.com/esteansm/" target="_blank" title="XXX en Instagram">
                <img src="img/logo-instagram-20-20.png" alt="Instagram" width="20" height="20" /></a>
            <a href="https://twitter.com/EsteanSm?s=08" target="_blank" title="XXX en Twitter">
                <img src="img/logo-twitter-20-20.gif" alt="Twitter" width="20" height="20" /></a>
        </footer>
    </div>
</body>
</html>
