﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Domain;
using System.Web.Configuration;
using System.Data;

namespace Interfaz
{
    public partial class ManageRole : System.Web.UI.Page
    {
        string connectionString;
        private RoleBusiness roleBusiness;
        private LogInBusiness logInBusiness;
        private AuditBusiness auditBusiness;
        private ColaboratorBusiness collaboratorBusiness;
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Lapsus_DB"].ConnectionString;
            roleBusiness = new RoleBusiness(connectionString);
            logInBusiness = new LogInBusiness(connectionString);
            auditBusiness = new AuditBusiness(connectionString);
            collaboratorBusiness = new ColaboratorBusiness(connectionString);

            formNewRole.Visible = false;

            Label2.Text = collaboratorBusiness.GetNameColaborator((string)Session["user"]);


            if (!IsPostBack) {
                
                //si no hay roles
                //se imprime en interfaz
                if (getData() == false)
                {
                    lbl_title.Text = "Aún no hay Roles registrados";
                    lbl_parra.Text = "";
                }
                else
                {
                    
                }
            }
        }//Page_Load

        //metodo encargado de cargar el gridView
        //si no hay registros de roles  el metodo retorna false
        //esto con el fin de cambiar la información de la página al cargar
        private bool getData()
        {
            roleBusiness = new RoleBusiness(connectionString);

            DataSet dataset = roleBusiness.GetAllRoles();

            //si el dataset esta vacio, o sea, la persona no tiene tareas registradas
            if (dataset.Tables[0].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                gd_role.DataSource = dataset;
                gd_role.DataBind();
                return true;
            }
        }//getData


        protected void btnRegisterRole_Click(object sender, EventArgs e)
        {
            formNewRole.Visible = true;
            btnRegisterRole.Visible = false;
        }

        protected void gd_role_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gd_role.EditIndex = e.NewEditIndex;

            getData();
        }

        protected void gd_role_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Session["idRole_Grid"] = gd_role.DataKeys[e.RowIndex].Values["idRole"].ToString();
            Response.Redirect("DeleteRol.aspx");
        }

        protected void gd_role_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //se instancia la clase para hacer uso de sus atributos
            Role role = new Role();
            Audit audit = new Audit();

            //se instancia la clase Business
            roleBusiness = new RoleBusiness(connectionString);

            System.Web.UI.WebControls.TextBox txtIdRole = gd_role.Rows[e.RowIndex].FindControl("txt_idRole") as System.Web.UI.WebControls.TextBox;
            System.Web.UI.WebControls.TextBox txtTypeRole = gd_role.Rows[e.RowIndex].FindControl("txt_typeRole") as System.Web.UI.WebControls.TextBox;

            //se asignan los valores de los campos al objeto
            role.IdRole = Int32.Parse(txtIdRole.Text);
            role.TypeRole = txtTypeRole.Text;
            role.AuditUser= (string)Session["user"];
            role.AuditDate= DateTime.Today.ToString();
            role.AuditAction = "Editar";

            //se edita el role mediante el Business
            roleBusiness.EditRole(role);

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Editar";
            audit.ActionTable = "Role";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            gd_role.EditIndex = -1;
            getData();
        }

        protected void gd_role_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gd_role.EditIndex = -1;

            getData();
        }

        protected void btn_Registrar_Click(object sender, EventArgs e)
        {
            //se instancia la clase para hacer uso de sus atributos
            Role role = new Role();
            Audit audit = new Audit();

            //se le asignan a los atributos los datos ingresados
            role.IdRole = Int32.Parse(txt_idRole.Text);
            role.TypeRole = txt_typeRole.Text;
            role.AuditUser= (string)Session["user"];
            role.AuditDate= DateTime.Today.ToString();
            role.AuditAction = "Insertar";

            //se inserta el registro
            roleBusiness.InsertRole(role);

            //se crea una variables de tipo Random
            //para su posterior uso
            Random random = new Random();

            //se llena el objeto Audit con los datos a insertar
            audit.ActionId = random.Next(100000, 999999);
            audit.ActionDate = DateTime.Today.ToString();
            audit.ActionMade = "Insertar";
            audit.ActionTable = "Role";
            audit.ActionUser = (string)Session["user"];
            audit.ActionUserRole = logInBusiness.GetRole(
                collaboratorBusiness.GetIdCardColaborator(
                    (string)Session["user"]));

            //se llama al Business y se inserta la nueva Auditoria
            auditBusiness.InsertAudit(audit);

            Response.Redirect("ManageRole.aspx");
        }//btn_Registrar_Click

        protected void btn_Cancelar_Click(object sender, EventArgs e)
        {
            formNewRole.Visible = false;
            btnRegisterRole.Visible = true;
        }
    }
}