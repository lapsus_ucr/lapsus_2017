﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Proyect
    {
        String idProyect;
        String name;
        String status;
        String auditUser;
        String auditDate;
        String auditAction;

        public Proyect()
        {
            this.idProyect = "";
            this.name = "";
            this.status = "";
            this.AuditUser = "";
            this.AuditDate = "";
            this.AuditAction = "";
        }


        public Proyect(string idProyect, string name, string status, string auditUser,
                       string auditDate, string auditAction)
        {
            this.idProyect = idProyect;
            this.name = name;
            this.status = status;
            this.AuditUser = auditUser;
            this.AuditDate = auditDate;
            this.AuditAction = auditAction;
        }

        public string IdProyect
        {
            get
            {
                return idProyect;
            }

            set
            {
                idProyect = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public string AuditUser
        {
            get
            {
                return auditUser;
            }

            set
            {
                auditUser = value;
            }
        }

        public string AuditDate
        {
            get
            {
                return auditDate;
            }

            set
            {
                auditDate = value;
            }
        }

        public string AuditAction
        {
            get
            {
                return auditAction;
            }

            set
            {
                auditAction = value;
            }
        }
    }//fin de clase
}//namespace
