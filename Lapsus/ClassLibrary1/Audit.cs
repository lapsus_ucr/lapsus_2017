﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Audit
    {
        int actionId;
        string actionDate;
        string actionMade;
        string actionTable;
        string actionUser;
        string actionUserRole;


        public Audit()
        {
            this.ActionId = 0;
            this.ActionDate = "";
            this.ActionMade = "";
            this.ActionTable = "";
            this.ActionUser = "";
            this.ActionUserRole = "";
        }//constructor

        public Audit(int actionId, string actionDate, string actionMade,
                     string actionTable, string actionUser, string actionUserRole)
        {
            this.ActionId = actionId;
            this.ActionDate = actionDate;
            this.ActionMade = actionMade;
            this.ActionTable = actionTable;
            this.ActionUser = actionUser;
            this.ActionUserRole = actionUserRole;
        }//constructor


        public int ActionId
        {
            get
            {
                return actionId;
            }

            set
            {
                actionId = value;
            }
        }

        public string ActionDate
        {
            get
            {
                return actionDate;
            }

            set
            {
                actionDate = value;
            }
        }

        public string ActionMade
        {
            get
            {
                return actionMade;
            }

            set
            {
                actionMade = value;
            }
        }

        public string ActionTable
        {
            get
            {
                return actionTable;
            }

            set
            {
                actionTable = value;
            }
        }

        public string ActionUser
        {
            get
            {
                return actionUser;
            }

            set
            {
                actionUser = value;
            }
        }

        public string ActionUserRole
        {
            get
            {
                return actionUserRole;
            }

            set
            {
                actionUserRole = value;
            }
        }

    }//class Audit
}
