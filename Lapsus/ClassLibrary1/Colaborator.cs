﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Colaborator
    {
        String idCard;
        String name;
        String lastName1;
        String lastName2;
        String email;
        String address;
        String phone;
        String job;
        String role;

        public Colaborator()
        {
            this.IdCard = "";
            this.Name = "";
            this.LastName1 = "";
            this.LastName2 = "";
            this.Email = "";
            this.Address = "";
            this.Phone = "";
            this.Job = "";
            this.Role = "";
        }

        public Colaborator(string idCard, string name, string lastName1, string lastName2, string email, string address, string phone, string job, string role)
        {
            this.IdCard = idCard;
            this.Name = name;
            this.LastName1 = lastName1;
            this.LastName2 = lastName2;
            this.Email = email;
            this.Address = address;
            this.Phone = phone;
            this.Job = job;
            this.Role = role;
        }


        public string IdCard
        {
            get
            {
                return idCard;
            }

            set
            {
                idCard = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string LastName1
        {
            get
            {
                return lastName1;
            }

            set
            {
                lastName1 = value;
            }
        }

        public string LastName2
        {
            get
            {
                return lastName2;
            }

            set
            {
                lastName2 = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }

            set
            {
                phone = value;
            }
        }

        public string Job
        {
            get
            {
                return job;
            }

            set
            {
                job = value;
            }
        }

        public string Role
        {
            get
            {
                return role;
            }

            set
            {
                role = value;
            }
        }
    }//fin de clase
}//namespace
