﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class ExtraHours
    {
        int idTask;
        string nameCategory;
        string nameProject;
        string taskDescription;
        string taskDate;
        string taskHours;
        string idCardCollaborator;

        public ExtraHours()
        {
            this.IdTask = 0;
            this.NameCategory = "";
            this.NameProject = "";
            this.TaskDescription = "";
            this.TaskDate = "";
            this.TaskHours = "";
            this.IdCardCollaborator = "";
        }//ExtraHours

        public ExtraHours(int idTask, string nameCategory, string nameProject,
                          string taskDescription, string taskDate, string taskHours,
                          string idCardCollaborator) {
            this.idTask = idTask;
            this.nameCategory = nameCategory;
            this.nameProject = nameProject;
            this.TaskDescription = taskDescription;
            this.taskDate = taskDate;
            this.taskHours = taskHours;
            this.idCardCollaborator = idCardCollaborator;
        }

        public int IdTask
        {
            get
            {
                return idTask;
            }

            set
            {
                idTask = value;
            }
        }

        public string NameCategory
        {
            get
            {
                return nameCategory;
            }

            set
            {
                nameCategory = value;
            }
        }

        public string NameProject
        {
            get
            {
                return nameProject;
            }

            set
            {
                nameProject = value;
            }
        }

        public string TaskDescription
        {
            get
            {
                return taskDescription;
            }

            set
            {
                taskDescription = value;
            }
        }

        public string TaskDate
        {
            get
            {
                return taskDate;
            }

            set
            {
                taskDate = value;
            }
        }

        public string TaskHours
        {
            get
            {
                return taskHours;
            }

            set
            {
                taskHours = value;
            }
        }

        public string IdCardCollaborator
        {
            get
            {
                return idCardCollaborator;
            }

            set
            {
                idCardCollaborator = value;
            }
        }
    }//Class
}//Namespace
