﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Tasks
    {
        String idTask;
        String idCategory;
        String idProyect;
        String description;
        String date;
        String hours;
        String taskType;
        String idCardColaborator;
        String auditUser;
        String auditDate;
        String auditAction;
        String taskExtraHours;

        public Tasks()
        {
            this.idTask = "";
            this.idCategory = "";
            this.idProyect = "";
            this.description = "";
            this.date = "";
            this.hours = "";
            this.taskType = "";
            this.idCardColaborator = "";
            this.auditUser = "";
            this.auditDate = "";
            this.auditAction = "";
            this.TaskExtraHours = "";
        }

        public Tasks(string idTask, string idCategory, string idProyect, 
                     string description, string date, string hours, 
                     string taskType ,string idCardColaborator, string auditUser,
                     string auditDate, string auditAction, string extraHours)
        {
            this.idTask = idTask;
            this.idCategory = idCategory;
            this.idProyect = idProyect;
            this.description = description;
            this.date = date;
            this.hours = hours;
            this.taskType = taskType;
            this.idCardColaborator = idCardColaborator;
            this.auditUser = auditUser;
            this.auditDate = auditDate;
            this.auditAction = auditAction;
            this.TaskExtraHours = extraHours;
        }

        public string IdTask
        {
            get
            {
                return idTask;
            }

            set
            {
                idTask = value;
            }
        }

        public string IdCategory
        {
            get
            {
                return idCategory;
            }

            set
            {
                idCategory = value;
            }
        }

        public string IdProyect
        {
            get
            {
                return idProyect;
            }

            set
            {
                idProyect = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public string Hours
        {
            get
            {
                return hours;
            }

            set
            {
                hours = value;
            }
        }

        public string IdCardColaborator
        {
            get
            {
                return idCardColaborator;
            }

            set
            {
                idCardColaborator = value;
            }
        }

        public string TaskType
        {
            get
            {
                return taskType;
            }

            set
            {
                taskType = value;
            }
        }

        public string AuditUser
        {
            get
            {
                return auditUser;
            }

            set
            {
                auditUser = value;
            }
        }

        public string AuditDate
        {
            get
            {
                return auditDate;
            }

            set
            {
                auditDate = value;
            }
        }

        public string AuditAction
        {
            get
            {
                return auditAction;
            }

            set
            {
                auditAction = value;
            }
        }

        public string TaskExtraHours
        {
            get
            {
                return taskExtraHours;
            }

            set
            {
                taskExtraHours = value;
            }
        }
    }
}
