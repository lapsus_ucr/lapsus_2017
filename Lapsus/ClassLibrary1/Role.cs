﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Role
    {

        int idRole;
        string typeRole;
        String auditUser;
        String auditDate;
        String auditAction;

        public Role() {
            this.idRole = 0;
            this.typeRole = "";
            this.AuditUser = "";
            this.AuditDate = "";
            this.AuditAction = "";
        }//constructor

        public Role(int idRole, string typeRole, string auditUser, string auditDate,
                    string auditAction) {
            this.idRole = idRole;
            this.typeRole = typeRole;
            this.auditUser = auditUser;
            this.auditDate = auditDate;
            this.auditAction = auditAction;
        }//constructor sobrecargado

        public int IdRole
        {
            get
            {
                return idRole;
            }

            set
            {
                idRole = value;
            }
        }

        public string TypeRole
        {
            get
            {
                return typeRole;
            }

            set
            {
                typeRole = value;
            }
        }

        public string AuditUser
        {
            get
            {
                return auditUser;
            }

            set
            {
                auditUser = value;
            }
        }

        public string AuditDate
        {
            get
            {
                return auditDate;
            }

            set
            {
                auditDate = value;
            }
        }

        public string AuditAction
        {
            get
            {
                return auditAction;
            }

            set
            {
                auditAction = value;
            }
        }
    }//Class Role
}
