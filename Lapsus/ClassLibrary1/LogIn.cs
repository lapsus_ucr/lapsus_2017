﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class LogIn
    {
        String user;
        String pass;
        String idCard;

        public LogIn()
        {
            this.user = "";
            this.pass = "";
            this.idCard = "";
        }

        public LogIn(String user, String pass, String idCard)
        {
            this.user = user;
            this.pass = pass;
            this.idCard = idCard;
        }

        public string User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }

        public string Pass
        {
            get
            {
                return pass;
            }

            set
            {
                pass = value;
            }
        }

        public string IdCard
        {
            get
            {
                return idCard;
            }

            set
            {
                idCard = value;
            }
        }
    }
}
