﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Category
    {
        String idCategory;
        String name;
        String auditUser;
        String auditDate;
        String auditAction;

        public Category()
        {
            this.idCategory = "";
            this.name = "";
            this.AuditUser = "";
            this.AuditDate = "";
            this.AuditAction = "";
        }

        public Category(string idCategory, string name, string auditUser,
                        string auditDate, string auditAction)
        {
            this.idCategory = idCategory;
            this.name = name;
            this.AuditUser = auditUser;
            this.AuditDate = auditDate;
            this.AuditAction = auditAction;
        }

        public string IdCategory
        {
            get
            {
                return idCategory;
            }

            set
            {
                idCategory = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string AuditUser
        {
            get
            {
                return auditUser;
            }

            set
            {
                auditUser = value;
            }
        }

        public string AuditDate
        {
            get
            {
                return auditDate;
            }

            set
            {
                auditDate = value;
            }
        }

        public string AuditAction
        {
            get
            {
                return auditAction;
            }

            set
            {
                auditAction = value;
            }
        }
    }//fin de clase
}//namespace
