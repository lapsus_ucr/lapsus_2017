﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Domain;

namespace Business
{
    public class AuditBusiness
    {
        private AuditData auditData;

        public AuditBusiness(string stringConnection)
        {
            this.auditData = new AuditData(stringConnection);
        }//constructor

        //metodo encargado de registrar una nueva accion de un usuario
        public Audit InsertAudit(Audit audit)
        {
            return auditData.InsertAudit(audit);
        }//InsertAudit
    }//AuditBusiness
}//Business
