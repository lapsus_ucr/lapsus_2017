﻿using Domain;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Business
{
    public class TaskBusiness
    {
        private TaskData taskData;

        public TaskBusiness(string stringConnection)
        {
            this.taskData = new TaskData(stringConnection);
        }//constructor

        public Tasks InsertTask(Tasks tasks)
        {
            return this.taskData.InsertTask(tasks);
        }//insertTask


        public Tasks EditTask(Tasks tasks, string oldTaskId)
        {
            return this.taskData.EditTask(tasks, oldTaskId);

        }//editTask

        public void RemoveTask(string idTask)
        {
            this.taskData.RemoveTask(idTask);
        }//removeTask

        public LinkedList<Tasks> GetTasks()
        {
            return taskData.GetTasks();
        }

        public LinkedList<Tasks> GetTask(string idTask)
        {
            return taskData.GetTask(idTask);
        }

        public DataSet getAllTasks()
        {
            return taskData.getAllTasks();
        }

        public DataSet getAllTaskByIdCard(string idCard)
        {
            return taskData.getAllTaskByIdCard(idCard);
        }

        public LinkedList<Tasks> getAllIDTaskByIdCard(string idCard)
        {
            return taskData.getAllIDTaskByIdCard(idCard);
        }

        public string returnIdTask()
        {
            return taskData.returnIdTask();
        }

        public bool existsTheId(string idTask)
        {
            return taskData.existsTheId(idTask);
        }

        public DataSet getDatesByRange(string desde, string hasta, string cedula)
        {
            return taskData.getDatesByRange(desde,hasta, cedula);
        }

        public string getTaskNameById(string idTask)
        {
            return taskData.getTaskNameById(idTask);
        }

        public string getCategoryNameByTaskId(string idTask)
        {
            return taskData.getCategoryNameByTaskId(idTask);
        }
        public LinkedList<Tasks> GetTaskForDuplicate(string idTask)
        {
            return taskData.GetTaskForDuplicate(idTask);
        }

        public LinkedList<Tasks> getDatesByRangeForReports(string idTask, string desde, string hasta)
        {
            return taskData.getDatesByRangeForReports(idTask,desde,hasta);
        }

        public DataSet getDatesByRange2(string desde, string hasta, string cedula)
        {
            return taskData.getDatesByRange2(desde, hasta, cedula);
        }

        public DataSet GetTaskLike(string nameCategory, string idCard)
        {
            return taskData.GetTaskLike(nameCategory,idCard);
        }

        public DataSet reporteUno()
        {
            return taskData.reporteUno();
        }

        public DataSet reporteDos()
        {
            return taskData.reporteDos();
        }

        public DataSet reporteTres()
        {
            return taskData.reporteTres();
        }
    }
}
