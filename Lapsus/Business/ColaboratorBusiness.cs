﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data.SqlClient;
using System.Data;

namespace Business
{
    public class ColaboratorBusiness
    {
        ColaboratorData colaboratorData;

        public ColaboratorBusiness(string connectionString)
        {
            colaboratorData = new ColaboratorData(connectionString);
        }//constructor

        public LinkedList<Colaborator> GetColaborators()
        {
            return colaboratorData.GetColaborators();
        }

        public Colaborator GetColaborator(string idCard)
        {
            return colaboratorData.GetColaborator(idCard);
        }

        public string GetIdCardColaborator(string user)
        {
            return colaboratorData.GetIdCardColaborator(user);
        }

        public string GetNameColaborator(string user)
        {
            return colaboratorData.GetNameColaborator(user);
        }

        public DataSet GetAllCollaborators()
        {
            return colaboratorData.GetAllCollaborators();
        }

        public string GetFullName(string idCard)
        {
            return colaboratorData.GetFullName(idCard);
        }

        public string GetNameColaboratorById(string idCard)
        {
            return colaboratorData.GetNameColaboratorById(idCard);
        }

        public string CollaboratorFullName(string idCard)
        {
            return colaboratorData.CollaboratorFullName(idCard);
        }
    }//class
}//namespace

    