﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Domain;
using System.Data;

namespace Business
{
    public class RoleBusiness
    {
        private RoleData roleData;

        public RoleBusiness(string connectionString)
        {
            this.roleData = new RoleData(connectionString);
        }//constructor

        public Role InsertRole(Role role)
        {
            return roleData.InsertRole(role);
        }

        public Role EditRole(Role role)
        {
            return roleData.EditRole(role);
        }

        public void removeRole(int idRole)
        {
            roleData.removeRole(idRole);
        }

        public DataSet GetAllRoles()
        {
            return roleData.GetAllRoles();
        }

        public bool existsTheId(int idRole)
        {
            return roleData.existsTheId(idRole);
        }
    }//class RoleBusiness
}//namespace
