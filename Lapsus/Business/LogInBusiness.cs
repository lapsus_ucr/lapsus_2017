﻿using Domain;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class LogInBusiness
    {
        LogInData logInData;

        public LogInBusiness(string connectionString)
        {
            logInData = new LogInData(connectionString);
        }//constructor

        public LogIn logIn(string usuario, string contrasena)
        {
            return this.logInData.logIn(usuario, contrasena);
        }//log in

        public string GetRole(string idCard)
        {
            return this.logInData.GetRole(idCard);
        }//GetRole
    }//class
}
