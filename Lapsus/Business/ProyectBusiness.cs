﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data;

namespace Business
{
    public class ProyectBusiness
    {

    	private ProyectData proyectData;

        public ProyectBusiness(string stringConnection)
        {
            this.proyectData = new ProyectData(stringConnection);
        }//constructor

        public Proyect InsertProyect(Proyect proyect)
        {
            return this.proyectData.InsertProyect(proyect);

        }//insertProyect


        public Proyect EditProyect(Proyect proyect)
        {
            return this.proyectData.EditProyect(proyect);

        }//editProyect

        public void RemoveProyect(string idProyect)
        {
            this.proyectData.RemoveProyect(idProyect);
        }//removeProyect

        public LinkedList<Proyect> GetProyects()
        {
            return proyectData.GetProyects();
        }//GetProyects

        public Proyect GetProyect(string idProyect)
        {
            return proyectData.GetProyect(idProyect);
        }//GetProyect

        public LinkedList<Proyect> GetProyectsIds()
        {
            return proyectData.GetProyectsIds();
        }//GetProyectsIds

        public LinkedList<Proyect> GetAllProjectNames()
        {
            return proyectData.GetAllProjectNames();
        }//GetAllProjectNames

        public DataSet GetAllProjectNamesDataSet()
        {
            return proyectData.GetAllProjectNamesDataSet();
        }

        public DataSet GetAllProjectsDataSet()
        {
            return proyectData.GetAllProjectsDataSet();
        }

        public bool existsTheId(string idProject)
        {
            return proyectData.existsTheId(idProject);
        }
    }//class
}//namespace

    