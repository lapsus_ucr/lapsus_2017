﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data;

namespace Business
{
    public class ExtraHoursBusiness
    {
        private ExtraHoursData extraHoursData;

        public ExtraHoursBusiness(string stringConnection)
        {
            this.extraHoursData = new ExtraHoursData(stringConnection);
        }//constructor

        public ExtraHours InsertExtraHours(ExtraHours extraHours)
        {
            return extraHoursData.InsertExtraHours(extraHours);
        }//InsertExtraHours

        public DataSet getHoursByRangeOfDates(string desde, string hasta, string cedula)
        {
            return extraHoursData.getHoursByRangeOfDates(desde,hasta, cedula);
        }
    }//class
}//namespace
