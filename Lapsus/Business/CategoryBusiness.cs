﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data;

namespace Business
{
    public class CategoryBusiness
    {
       // private string connectionString;
        private CategoryData categoryData;

        public CategoryBusiness(string connectionString)
        {
            this.categoryData = new CategoryData(connectionString);
        }//constructor

        public Category InsertCategory(Category category)
        {
            return this.categoryData.InsertCategory(category);

        }//insertCategory

        public Category EditCategory(Category category)
        {
            return this.categoryData.EditCategory(category);

        }//editCategory

        public void RemoveCategory(string idCategory)
        {
            this.categoryData.RemoveCategory(idCategory);
        }//eliminar

        public LinkedList<Category> GetCategories()
        {
            return this.categoryData.GetCategories();
        }

        public Category GetCategory(string idCategory)
        {
            return this.categoryData.GetCategory(idCategory);
        }

        public LinkedList<Category> GetCategoriesIds()
        {
            return this.categoryData.GetCategoriesIds();
        }

        public string GetCategoryIdByName(string name)
        {
            return categoryData.GetCategoryIdByName(name);
        }

        public LinkedList<Category> GetCategoriesNames()
        {
            return categoryData.GetCategoriesNames();
        }

        public DataSet GetAllCategoriesDataSet()
        {
            return categoryData.GetAllCategoriesDataSet();
        }

        public bool existsTheId(string idCategory)
        {
            return categoryData.existsTheId(idCategory);
        }
    }//class
}//namespace

   