﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.SqlClient;
using System.Data;

namespace Data
{
    public class RoleData
    {

        //attributes
        private string stringConnection;

        public RoleData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        //metodo con el cual se registrara la tabla Rol
        public Role InsertRole(Role role)
        {
            String slqProcedureInsertRole = "insertRole";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertRole = new SqlCommand(slqProcedureInsertRole, connection);
            commandInsertRole.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertRole.Parameters.Add(new SqlParameter("@idRole", role.IdRole));
            commandInsertRole.Parameters.Add(new SqlParameter("@typeRole", role.TypeRole));
            commandInsertRole.Parameters.Add(new SqlParameter("@auditUser", role.AuditUser));
            commandInsertRole.Parameters.Add(new SqlParameter("@auditDate", role.AuditDate));
            commandInsertRole.Parameters.Add(new SqlParameter("@auditAction", role.AuditAction));
            commandInsertRole.Connection.Open();
            commandInsertRole.ExecuteNonQuery();
            commandInsertRole.Connection.Close();

            return role;
        }//InsertRole

        public Role EditRole(Role role)
        {
            String slqProcedureEditRole = "EditRole";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandEditRole = new SqlCommand(slqProcedureEditRole, connection);
            commandEditRole.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandEditRole.Parameters.Add(new SqlParameter("@idRole", role.IdRole));
            commandEditRole.Parameters.Add(new SqlParameter("@typeRole", role.TypeRole));
            commandEditRole.Parameters.Add(new SqlParameter("@auditUser", role.AuditUser));
            commandEditRole.Parameters.Add(new SqlParameter("@auditDate", role.AuditDate));
            commandEditRole.Parameters.Add(new SqlParameter("@auditAction", role.AuditAction));
            commandEditRole.Connection.Open();
            commandEditRole.ExecuteNonQuery();
            commandEditRole.Connection.Close();
            return role;
        }//editRole

        //metodo utilizado para eliminar un Rol
        //esto mediante su id
        public void removeRole(int idRole)
        {
            String slqProcedureRemoveRole = "removeRole";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandRemoveRole = new SqlCommand(slqProcedureRemoveRole, connection);
            commandRemoveRole.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandRemoveRole.Parameters.Add(new SqlParameter("@idRole", idRole));
            commandRemoveRole.Connection.Open();
            commandRemoveRole.ExecuteNonQuery();
            commandRemoveRole.Connection.Close();
        }//remove

        //Se devuelve un dataset lleno con todos los atributos
        public DataSet GetAllRoles()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "getAllRoles";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Rol");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //GetAllRoles

        //este metodo se encarga de buscar en la Tabla Rol si el 
        //idRole ingresado por parametro existe en ella.
        public bool existsTheId(int idRole)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "getAllRolesId";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsCategory = new DataSet();
            daTask.Fill(dsCategory, "Rol");
            conexion.Close();

            DataRowCollection drCategory = dsCategory.Tables["Rol"].Rows;

            Role currentRole = new Role();

            foreach (DataRow drActual in drCategory)
            {
                currentRole.IdRole = Int32.Parse(drActual["idRole"].ToString());
                if (currentRole.IdRole==idRole)
                {
                    return true;
                }//if
            }//foreach
            return false;
        }//existsTheId

    }//Class RoleData
}
