﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data.SqlClient;
using System.Data;

namespace Data
{
   public class CategoryData
    {

        //atributos
        private string stringConnection;

        public CategoryData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor


        public Category InsertCategory(Category category)
        {
            String slqProcedureInsertCategory = "InsertCategory";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertCategory = new SqlCommand(slqProcedureInsertCategory, connection);
            commandInsertCategory.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertCategory.Parameters.Add(new SqlParameter("@idCategory", category.IdCategory));
            commandInsertCategory.Parameters.Add(new SqlParameter("@name", category.Name));
            commandInsertCategory.Parameters.Add(new SqlParameter("@auditUser", category.AuditUser));
            commandInsertCategory.Parameters.Add(new SqlParameter("@auditDate", category.AuditDate));
            commandInsertCategory.Parameters.Add(new SqlParameter("@auditAction", category.AuditAction));
            commandInsertCategory.Connection.Open();
            commandInsertCategory.ExecuteNonQuery();
            commandInsertCategory.Connection.Close();
            return category;
        }//insert

        public Category EditCategory(Category category)
        {
            String slqProcedureEditCategory = "EditCategory";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandEditCategory = new SqlCommand(slqProcedureEditCategory, connection);
            commandEditCategory.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandEditCategory.Parameters.Add(new SqlParameter("@idCategory", category.IdCategory));
            commandEditCategory.Parameters.Add(new SqlParameter("@name", category.Name));
            commandEditCategory.Parameters.Add(new SqlParameter("@auditUser", category.AuditUser));
            commandEditCategory.Parameters.Add(new SqlParameter("@auditDate", category.AuditDate));
            commandEditCategory.Parameters.Add(new SqlParameter("@auditAction", category.AuditAction));
            commandEditCategory.Connection.Open();
            commandEditCategory.ExecuteNonQuery();
            commandEditCategory.Connection.Close();
            return category;
        }//edit

        public void RemoveCategory(string idCategory)
        {
            String slqProcedureRemoveCategory = "RemoveCategory";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandRemoveCategory = new SqlCommand(slqProcedureRemoveCategory, connection);
            commandRemoveCategory.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandRemoveCategory.Parameters.Add(new SqlParameter("@idCategory", idCategory));
            commandRemoveCategory.Connection.Open();
            commandRemoveCategory.ExecuteNonQuery();
            commandRemoveCategory.Connection.Close();
        }//remove

        public LinkedList<Category> GetCategories()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetCategories;";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Category");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Category"].Rows;

            LinkedList<Category> resultCategory = new LinkedList<Category>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Category currentCategory = new Category();
                currentCategory.IdCategory = currentRow["idCategory"].ToString();
                currentCategory.Name = currentRow["name"].ToString();

                //insert in the list at last
                resultCategory.AddLast(currentCategory);
            } // foreach

            return resultCategory;
        }

        public Category GetCategory(string idCategory)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetCategory = "GetCategory";
            SqlCommand commandGetCategory = new SqlCommand(slqProcedureGetCategory, conexion);
            commandGetCategory.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetCategory.Parameters.Add(new SqlParameter("@idCategory", idCategory)); //manda el parametro a la bd
            SqlDataAdapter daCategory = new SqlDataAdapter(commandGetCategory);
            DataSet dsCategory = new DataSet();
            daCategory.Fill(dsCategory, "Category");
            conexion.Close();

            DataRowCollection drCategory = dsCategory.Tables["Category"].Rows;
            Category categoryFinded = new Category();

            foreach (DataRow drCurrentCategory in drCategory)
            {
                categoryFinded.IdCategory= drCurrentCategory["idCategory"].ToString();
                categoryFinded.Name = drCurrentCategory["name"].ToString();
            }
            return categoryFinded;
        }//GetCategory

        public LinkedList<Category> GetCategoriesIds()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetCategoryIDS;";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Category");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Category"].Rows;

            LinkedList<Category> resultCategory = new LinkedList<Category>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Category currentCategory = new Category();
                currentCategory.IdCategory = currentRow["idCategory"].ToString();
                //insert in the list at last
                resultCategory.AddLast(currentCategory);
            } // foreach

            return resultCategory;
        }

        public string GetCategoryIdByName(string name)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetCategory = "GetCategoryIdByName";
            SqlCommand commandGetCategory = new SqlCommand(slqProcedureGetCategory, conexion);
            commandGetCategory.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetCategory.Parameters.Add(new SqlParameter("@name", name)); //manda el parametro a la bd
            SqlDataAdapter daCategory = new SqlDataAdapter(commandGetCategory);
            DataSet dsCategory = new DataSet();
            daCategory.Fill(dsCategory, "Category");
            conexion.Close();

            DataRowCollection drCategory = dsCategory.Tables["Category"].Rows;
            Category categoryFinded = new Category();
            string idByName = "";

            foreach (DataRow drCurrentCategory in drCategory)
            {
                idByName = drCurrentCategory["idCategory"].ToString();
            }
            return idByName;
        }//GetCategory

        public LinkedList<Category> GetCategoriesNames()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetCategoryNames;";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Category");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Category"].Rows;

            LinkedList<Category> resultCategory = new LinkedList<Category>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Category currentCategory = new Category();
                currentCategory.Name = currentRow["name"].ToString();
                //insert in the list at last
                resultCategory.AddLast(currentCategory);
            } // foreach

            return resultCategory;
        }

        public DataSet GetAllCategoriesDataSet()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetCategories";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Category");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //GetAllCategoriesDataSet()


        //este metodo se encarga de buscar en la Tabla Category si el 
        //idCategory ingresado por parametro existe en ella.
        public bool existsTheId(string idCategory)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "GetCategoryIDS";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsCategory = new DataSet();
            daTask.Fill(dsCategory, "Category");
            conexion.Close();

            DataRowCollection drCategory = dsCategory.Tables["Category"].Rows;

            Category currentCategory = new Category();

            foreach (DataRow drActual in drCategory)
            {
                currentCategory.IdCategory = drActual["idCategory"].ToString();
                if (currentCategory.IdCategory.Equals(idCategory))
                {
                    return true;
                }//if
            }//foreach
            return false;
        }//existsTheId

    }//class
}//namespace
