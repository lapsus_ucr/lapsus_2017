﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.SqlClient;

namespace Data
{
    public class AuditData
    {
        //attributes
        private string stringConnection;

        public AuditData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        //metodo con el cual se registrara un cambio en la tabla Audit
        //esto cada vez que un usuario realiza alguna accion en la 
        //aplicacion
        public Audit InsertAudit(Audit audit)
        {
            String slqProcedureInsertTask = "insertAudit";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertAudit = new SqlCommand(slqProcedureInsertTask, connection);
            commandInsertAudit.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionId", audit.ActionId));
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionDate", audit.ActionDate));
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionMade", audit.ActionMade));
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionTable", audit.ActionTable));
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionUser", audit.ActionUser));
            commandInsertAudit.Parameters.Add(new SqlParameter("@actionUserRole", audit.ActionUserRole));
            commandInsertAudit.Connection.Open();
            commandInsertAudit.ExecuteNonQuery();
            commandInsertAudit.Connection.Close();

            return audit;
        }//InsertAudit
    }//class AuditData
}//namespace Data
