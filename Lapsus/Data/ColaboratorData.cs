﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Data;
using System.Data.SqlClient;
using System.Data;

namespace Data
{
    public class ColaboratorData
    {
        //atributos
        private string stringConnection;

        public ColaboratorData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        public LinkedList<Colaborator> GetColaborators()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetColaborators";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Collaborator");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Collaborator"].Rows;

            LinkedList<Colaborator> resultColaborator = new LinkedList<Colaborator>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Colaborator currentColaborator = new Colaborator();
                currentColaborator.Name = currentRow["name"].ToString();
                currentColaborator.LastName1 = currentRow["lastName1"].ToString();
                currentColaborator.LastName2 = currentRow["lastName2"].ToString();
                currentColaborator.Email = currentRow["email"].ToString();
                currentColaborator.Address = currentRow["userAddress"].ToString();
                currentColaborator.Phone = currentRow["phone"].ToString();
                currentColaborator.IdCard = currentRow["idCard"].ToString();
                currentColaborator.Job = currentRow["job"].ToString();
                currentColaborator.Role = currentRow["userRole"].ToString();

                //insert in the list at last
                resultColaborator.AddLast(currentColaborator);
            } // foreach

            return resultColaborator;
        }


        public Colaborator GetColaborator(string idCard)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "GetColaborator";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@idCard", idCard)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator colaboratorFinded = new Colaborator();
            

            foreach (DataRow drActual in drColaborator)
            {
                colaboratorFinded.Name = drActual["name"].ToString();
                colaboratorFinded.LastName1 = drActual["lastName1"].ToString();
                colaboratorFinded.LastName2 = drActual["lastName2"].ToString();
                colaboratorFinded.Email = drActual["email"].ToString();
                colaboratorFinded.Address = drActual["userAddress"].ToString();
                colaboratorFinded.Phone = drActual["phone"].ToString();
                colaboratorFinded.IdCard = drActual["idCard"].ToString();
                colaboratorFinded.Job = drActual["job"].ToString();
                colaboratorFinded.Role = drActual["userRole"].ToString();
            }
            return colaboratorFinded;
        }//GetColaborator

        public string GetIdCardColaborator(string user)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "GetIdCard";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@user", user)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string identificationCard = "";

            foreach (DataRow drActual in drColaborator)
            {
                identificationCard = drActual["idCard"].ToString();
            }
            return identificationCard;
        }//GetIdCardColaborator

        public string GetNameColaborator(string user)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "GetUserName";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@user", user)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string name = "";

            foreach (DataRow drActual in drColaborator)
            {
                name = drActual["name"].ToString();
            }
            return name;
        }//GetIdCardColaborator

        //metodo encargado de obtener todos los colaboradores 
        public DataSet GetAllCollaborators()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetAllCollaborators";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Collaborator");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodosLosCursos

        public string GetFullName(string idCard) {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "GetFullName";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@idCard", idCard)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string fullName = "";

            foreach (DataRow drActual in drColaborator)
            {
                fullName = drActual["FullName"].ToString();
            }
            return fullName;
        }


        public string GetNameColaboratorById(string idCard)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "GetUserNameById";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@idCard", idCard)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string name = "";

            foreach (DataRow drActual in drColaborator)
            {
                name = drActual["name"].ToString();
            }
            return name;
        }//GetIdCardColaborator

        //con este metodo se retorna el nombre y los dos apellidos de un colaborador
        //se busca mediante su cedula
        public string CollaboratorFullName(string idCard)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "CollaboratorFullName";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@idCard", idCard)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string name = "";
            string ape1 = "";
            string ape2 = "";

            foreach (DataRow drActual in drColaborator)
            {
                name = drActual["name"].ToString();
                ape1 = drActual["lastName1"].ToString();
                ape2 = drActual["lastName2"].ToString();
            }
            return name+" "+ape1+" "+ape2;
        }
    }//clase
}//namespace
