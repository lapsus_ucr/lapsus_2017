﻿using Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class TaskData
    {
        //attributes
        private string stringConnection;

        public TaskData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        public Tasks InsertTask(Tasks tasks)
        {
            String slqProcedureInsertTask = "InsertTask";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertTask = new SqlCommand(slqProcedureInsertTask, connection);
            commandInsertTask.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertTask.Parameters.Add(new SqlParameter("@idTask", tasks.IdTask));
            commandInsertTask.Parameters.Add(new SqlParameter("@nameCategory", tasks.IdCategory));
            commandInsertTask.Parameters.Add(new SqlParameter("@nameProyect", tasks.IdProyect));
            commandInsertTask.Parameters.Add(new SqlParameter("@description", tasks.Description));
            commandInsertTask.Parameters.Add(new SqlParameter("@date", tasks.Date));
            commandInsertTask.Parameters.Add(new SqlParameter("@hours", tasks.Hours));
            commandInsertTask.Parameters.Add(new SqlParameter("@extraHhours", tasks.TaskExtraHours));
            commandInsertTask.Parameters.Add(new SqlParameter("@idCardCollaborator", tasks.IdCardColaborator));
            commandInsertTask.Parameters.Add(new SqlParameter("@auditUser", tasks.AuditUser));
            commandInsertTask.Parameters.Add(new SqlParameter("@auditDate", tasks.AuditDate));
            commandInsertTask.Parameters.Add(new SqlParameter("@auditAction", tasks.AuditAction));
            commandInsertTask.Connection.Open();
            commandInsertTask.ExecuteNonQuery();
            commandInsertTask.Connection.Close();

            return tasks;
        }//insert

        public Tasks EditTask(Tasks tasks, string oldTaskId)
        {
            String slqProcedureEditTask = "EditTask";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandEditTask = new SqlCommand(slqProcedureEditTask, connection);
            commandEditTask.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandEditTask.Parameters.Add(new SqlParameter("@idOldTask", oldTaskId));
            commandEditTask.Parameters.Add(new SqlParameter("@idTask", tasks.IdTask));
            commandEditTask.Parameters.Add(new SqlParameter("@nameCategory", tasks.IdCategory));
            commandEditTask.Parameters.Add(new SqlParameter("@nameProyect", tasks.IdProyect));
            commandEditTask.Parameters.Add(new SqlParameter("@description", tasks.Description));
            commandEditTask.Parameters.Add(new SqlParameter("@hours", tasks.Hours));
            commandEditTask.Parameters.Add(new SqlParameter("@extraHhours", tasks.TaskExtraHours));
            commandEditTask.Parameters.Add(new SqlParameter("@idCardCollaborator", tasks.IdCardColaborator));
            commandEditTask.Parameters.Add(new SqlParameter("@auditUser", tasks.AuditUser));
            commandEditTask.Parameters.Add(new SqlParameter("@auditDate", tasks.AuditDate));
            commandEditTask.Parameters.Add(new SqlParameter("@auditAction", tasks.AuditAction));
            commandEditTask.Connection.Open();
            commandEditTask.ExecuteNonQuery();
            commandEditTask.Connection.Close();

            return tasks;
        }//edit


        public void RemoveTask(string idTask)
        {
            String slqProcedureRemoveTask = "RemoveTask";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandRemoveTask = new SqlCommand(slqProcedureRemoveTask, connection);
            commandRemoveTask.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandRemoveTask.Parameters.Add(new SqlParameter("@idTask", idTask));
            commandRemoveTask.Connection.Open();
            commandRemoveTask.ExecuteNonQuery();
            commandRemoveTask.Connection.Close();

        }//remove

        public LinkedList<Tasks> GetTasks()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetTasks";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Tasks");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Tasks"].Rows;

            LinkedList<Tasks> resultTask = new LinkedList<Tasks>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdTask = currentRow["idTask"].ToString();
                currentTask.IdCategory = currentRow["nameCategory"].ToString();
                currentTask.IdProyect = currentRow["nameProyect"].ToString();
                currentTask.Description = currentRow["taskDescription"].ToString();
                currentTask.Date = currentRow["taskDate"].ToString();
                currentTask.Hours = currentRow["taskHours"].ToString();
                currentTask.IdCardColaborator = currentRow["idCardCollaborator"].ToString();

                //insert in the list at last
                resultTask.AddLast(currentTask);
            } // foreach

            return resultTask;
        }


        public LinkedList<Tasks> GetTask(string idTask)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "GetTask";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idTask", idTask));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            LinkedList<Tasks> taskFinded = new LinkedList<Tasks>();

            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdTask = drActual["idTask"].ToString();
                currentTask.IdCategory = drActual["nameCategory"].ToString();
                currentTask.IdProyect = drActual["nameProject"].ToString();
                currentTask.Description = drActual["taskDescription"].ToString();
                currentTask.Date = drActual["taskDate"].ToString();
                currentTask.Hours = drActual["taskHours"].ToString();
                currentTask.IdCardColaborator = drActual["idCardCollaborator"].ToString();
                taskFinded.AddLast(currentTask);
            }
            return taskFinded;
        }

        //se obtienen todas las tareas; se almacenan en un DataSet
        //para poder imprimirlos facilmente
        public DataSet getAllTasks()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetTasks";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Tasks");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodosLosCursos

        //se obtienen todos las tareas de un colaborador en especifico
        public DataSet getAllTaskByIdCard(string idCard)
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "getSomeTaskAttributes";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            cmdObtener.Parameters.Add(new SqlParameter("@idCard", idCard));

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Tasks");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodas las tareas

        public LinkedList<Tasks> getAllIDTaskByIdCard(string idCard)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "GetIDTasks";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idCard", idCard));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            LinkedList<Tasks> taskFinded = new LinkedList<Tasks>();


            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdTask = drActual["idTask"].ToString();
                taskFinded.AddLast(currentTask);
            }
            return taskFinded;
        }//getAllIDTaskByIdCard

        public string returnIdTask()
        {
            Random random = new Random();

            int numberId = random.Next(100000, 999999);

            return "TK-" + numberId.ToString();
        }//returnIdTask

        //este metodo se encarga de buscar en la Tabla Tasks si el 
        //idTask ingresado por parametro existe en ella.
        public bool existsTheId(string idTask)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "existsTask";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idTask", idTask));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;

            Tasks currentTask = new Tasks();

            foreach (DataRow drActual in drTask)
            {
                currentTask.IdTask = drActual["idTask"].ToString();
                if (currentTask.IdTask.Equals(idTask)) {
                    return true;
                }//if
            }//foreach
            return false;
        }//existsTheId

        //metodo encargado de traer todas las tareas que se encuentran
        //en el rango de fechas dado por parametro
        public DataSet getDatesByRange(string desde, string hasta, string cedula)
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "getDatesByRange";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            cmdObtener.Parameters.Add(new SqlParameter("@fechaInicio", desde));
            cmdObtener.Parameters.Add(new SqlParameter("@fechaFin", hasta));
            cmdObtener.Parameters.Add(new SqlParameter("@cedula", cedula));

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Tasks");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodas las tareas

        //se obtiene el nombre del proyecto de la tarea a buscar
        public string getTaskNameById(string idTask)
        {
            long newId = int.Parse(idTask);

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "getTaskNameById";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idTask", newId));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            string taskFinded = "";

            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdProyect = drActual["nameProject"].ToString();
                taskFinded = currentTask.IdProyect;
            }
            return taskFinded;
        }//getTaskNameById

        //se obtiene el nombre de la categoria de la tarea a buscar
        public string getCategoryNameByTaskId(string idTask)
        {
            long newId = int.Parse(idTask);

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "getCategoryNameByTaskId";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idTask", newId));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            string taskFinded = "";

            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdCategory = drActual["nameCategory"].ToString();
                taskFinded = currentTask.IdCategory;
            }
            return taskFinded;
        }//getCategoryNameByTaskId

        //con este metodo se traen los atributos de la fila a duplicar
        //esto en las vista de Admin y User
        public LinkedList<Tasks> GetTaskForDuplicate(string idTask)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "GetTaskForDuplicate";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@idTask", idTask));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            LinkedList<Tasks> taskFinded = new LinkedList<Tasks>();

            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdCategory = drActual["nameCategory"].ToString();
                currentTask.IdProyect = drActual["nameProject"].ToString();
                currentTask.Description = drActual["taskDescription"].ToString();
                currentTask.Date = drActual["taskDate"].ToString();
                currentTask.Hours = drActual["taskRegularHours"].ToString();
                currentTask.TaskExtraHours = drActual["taskExtraHours"].ToString();
                taskFinded.AddLast(currentTask);
            }
            return taskFinded;
        }//GetTaskForDuplicate

        //metodo que filtra las tareas de un colaborador por un rango de fechas 
        public LinkedList<Tasks> getDatesByRangeForReports(string idTask, string desde, string hasta)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetTask = "getDatesByRangeForReports";
            SqlCommand commandGetTask = new SqlCommand(slqProcedureGetTask, conexion);
            commandGetTask.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetTask.Parameters.Add(new SqlParameter("@cedula", idTask));
            commandGetTask.Parameters.Add(new SqlParameter("@fechaInicio", desde));
            commandGetTask.Parameters.Add(new SqlParameter("@fechaFin", hasta));
            SqlDataAdapter daTask = new SqlDataAdapter(commandGetTask);
            DataSet dsTask = new DataSet();
            daTask.Fill(dsTask, "Tasks");
            conexion.Close();

            DataRowCollection drTask = dsTask.Tables["Tasks"].Rows;
            LinkedList<Tasks> taskFinded = new LinkedList<Tasks>();

            foreach (DataRow drActual in drTask)
            {
                Tasks currentTask = new Tasks();
                currentTask.IdCategory = drActual["nameCategory"].ToString();
                currentTask.IdProyect = drActual["nameProject"].ToString();
                currentTask.Description = drActual["taskDescription"].ToString();
                currentTask.Date = drActual["taskDate"].ToString();
                currentTask.Hours = drActual["taskRegularHours"].ToString();
                currentTask.Hours = drActual["taskExtraHours"].ToString();
                taskFinded.AddLast(currentTask);
            }
            return taskFinded;
        }//getDatesByRangeForReports

        public DataSet getDatesByRange2(string desde, string hasta, string cedula)
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "getDatesByRangeForReports";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            cmdObtener.Parameters.Add(new SqlParameter("@fechaInicio", desde));
            cmdObtener.Parameters.Add(new SqlParameter("@fechaFin", hasta));
            cmdObtener.Parameters.Add(new SqlParameter("@cedula", cedula));

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Tasks");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodas las tareas

        //se obtienen todas las tareas que tengan una categoria igual a lo que se esta
        //ingresando
        //CONSULTA LIKE
        public DataSet GetTaskLike(string nameCategory, string idCard)
        {

            int idCardCollaborator = int.Parse(idCard);

            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetTaskLike";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            cmdObtener.Parameters.Add(new SqlParameter("@nameCategory", nameCategory));
            cmdObtener.Parameters.Add(new SqlParameter("@idCard", idCardCollaborator));

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Tasks");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodas las tareas

        //se obtienen los datos generados en la VISTA: datos_reporte_1
        public DataSet reporteUno()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "get_view1_data";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "datos_reporte_1");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //obtenerTodosLosCursos   

        //se obtienen los datos generados en la VISTA: datos_reporte_2
        public DataSet reporteDos()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "get_view2_data";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "datos_reporte_2");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //obtenerTodosLosCursos 

        //se obtienen los datos generados en la VISTA: datos_reporte_3
        public DataSet reporteTres()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "get_view3_data";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "datos_reporte_3");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //obtenerTodosLosCursos 

    }//Class
}//Namespace
