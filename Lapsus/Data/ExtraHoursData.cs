﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.SqlClient;
using System.Data;

namespace Data
{
    public class ExtraHoursData
    {

        //attributes
        private string stringConnection;

        public ExtraHoursData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        public ExtraHours InsertExtraHours(ExtraHours extraHours)
        {
            String slqProcedureInsertExtraHours = "InsertExtraHours";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertExtraHours = new SqlCommand(slqProcedureInsertExtraHours, connection);
            commandInsertExtraHours.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@idTask", extraHours.IdTask));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@nameCategory", extraHours.NameCategory));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@nameProject", extraHours.NameProject));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@taskDescription", extraHours.TaskDescription));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@taskDate", extraHours.TaskDate));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@taskHours", extraHours.TaskHours));
            commandInsertExtraHours.Parameters.Add(new SqlParameter("@idCardCollaborator", extraHours.IdCardCollaborator));
            commandInsertExtraHours.Connection.Open();
            commandInsertExtraHours.ExecuteNonQuery();
            commandInsertExtraHours.Connection.Close();

            return extraHours;
        }//InsertAudit

        //metodo encargado de traer todas las tareas que se encuentran
        //en el rango de fechas dado por parametro
        public DataSet getHoursByRangeOfDates(string desde, string hasta, string cedula)
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "getHoursByRangeOfDates";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            cmdObtener.Parameters.Add(new SqlParameter("@fechaInicio", desde));
            cmdObtener.Parameters.Add(new SqlParameter("@fechaFin", hasta));
            cmdObtener.Parameters.Add(new SqlParameter("@cedula", cedula));

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "ExtraHours");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;

        } //obtenerTodas las tareas

    }//class
}//namespace
