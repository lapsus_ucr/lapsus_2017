﻿using System;
using Domain;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class LogInData
    {
        //atributos
        private string stringConnection;

        public LogInData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        public LogIn logIn(string user, string pass)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureLog_In_procedure = "Log_In_procedure";
            SqlCommand commandLog_In_procedure = new SqlCommand(slqProcedureLog_In_procedure, conexion);
            commandLog_In_procedure.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandLog_In_procedure.Parameters.Add(new SqlParameter("@user", user)); //manda el parametro a la bd
            commandLog_In_procedure.Parameters.Add(new SqlParameter("@pass", pass)); //manda el parametro a la bd
            SqlDataAdapter daLogIn = new SqlDataAdapter(commandLog_In_procedure);
            DataSet dsLogIn = new DataSet();
            daLogIn.Fill(dsLogIn, "Log_In");
            conexion.Close();

            DataRowCollection drLogIn = dsLogIn.Tables["Log_In"].Rows;
            LogIn userFinded = new LogIn();

            foreach (DataRow drCurrentUser in drLogIn)
            {
                userFinded.User = drCurrentUser["collaboratorUser"].ToString();
                userFinded.Pass = drCurrentUser["pass"].ToString();
                userFinded.IdCard = drCurrentUser["idCard"].ToString();
            }
            return userFinded;
        }//clase


        public string GetRole(string idCard)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetColaborator = "selectRole";
            SqlCommand commandGetColaborator = new SqlCommand(slqProcedureGetColaborator, conexion);
            commandGetColaborator.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetColaborator.Parameters.Add(new SqlParameter("@idCard", idCard)); //manda el parametro a la bd
            SqlDataAdapter daColaborator = new SqlDataAdapter(commandGetColaborator);
            DataSet dsColaborator = new DataSet();
            daColaborator.Fill(dsColaborator, "Collaborator");
            conexion.Close();

            DataRowCollection drColaborator = dsColaborator.Tables["Collaborator"].Rows;
            Colaborator idCardFinded = new Colaborator();

            string role = "";

            foreach (DataRow drActual in drColaborator)
            {
                role = drActual["userIdRole"].ToString();
            }
            return role;
        }//GetIdCardColaborator

    }
}
