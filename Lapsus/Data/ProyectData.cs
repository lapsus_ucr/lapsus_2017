﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using System.Data.SqlClient;
using System.Data;

namespace Data
{
   public class ProyectData
    {
        //attributes
        private string stringConnection;

        public ProyectData(string stringConnection)
        {
            this.stringConnection = stringConnection;
        }//constructor

        public Proyect InsertProyect(Proyect proyect)
        {
            String slqProcedureInsertProyect = "InsertProyect";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandInsertProyect = new SqlCommand(slqProcedureInsertProyect, connection);
            commandInsertProyect.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandInsertProyect.Parameters.Add(new SqlParameter("@idProject", proyect.IdProyect));
            commandInsertProyect.Parameters.Add(new SqlParameter("@name", proyect.Name));
            commandInsertProyect.Parameters.Add(new SqlParameter("@auditUser", proyect.AuditUser));
            commandInsertProyect.Parameters.Add(new SqlParameter("@auditDate", proyect.AuditDate));
            commandInsertProyect.Parameters.Add(new SqlParameter("@auditAction", proyect.AuditAction));
            commandInsertProyect.Connection.Open();
            commandInsertProyect.ExecuteNonQuery();
            commandInsertProyect.Connection.Close();

            return proyect;
        }//insert

        public Proyect EditProyect(Proyect proyect)
        {
            String slqProcedureEditProyect = "EditProyect";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandEditProyect = new SqlCommand(slqProcedureEditProyect, connection);
            commandEditProyect.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandEditProyect.Parameters.Add(new SqlParameter("@idProject", proyect.IdProyect));
            commandEditProyect.Parameters.Add(new SqlParameter("@name", proyect.Name));
            commandEditProyect.Parameters.Add(new SqlParameter("@status", proyect.Status));
            commandEditProyect.Parameters.Add(new SqlParameter("@auditUser", proyect.AuditUser));
            commandEditProyect.Parameters.Add(new SqlParameter("@auditDate", proyect.AuditDate));
            commandEditProyect.Parameters.Add(new SqlParameter("@auditAction", proyect.AuditAction));
            commandEditProyect.Connection.Open();
            commandEditProyect.ExecuteNonQuery();
            commandEditProyect.Connection.Close();

            return proyect;
        }//edit


        public void RemoveProyect(string idProyect)
        {
            String slqProcedureRemoveProyect = "RemoveProyect";
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand commandRemoveProyect = new SqlCommand(slqProcedureRemoveProyect, connection);
            commandRemoveProyect.CommandType = System.Data.CommandType.StoredProcedure;
            //parámetros de entrada
            commandRemoveProyect.Parameters.Add(new SqlParameter("@idProject", idProyect));
            commandRemoveProyect.Connection.Open();
            commandRemoveProyect.ExecuteNonQuery();
            commandRemoveProyect.Connection.Close();

        }//remove

        public LinkedList<Proyect> GetProyects()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetProyects";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Project");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Project"].Rows;

            LinkedList<Proyect> resultProyect = new LinkedList<Proyect>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Proyect currentProject = new Proyect();
                currentProject.IdProyect = currentRow["idProject"].ToString();
                currentProject.Name = currentRow["name"].ToString();
                currentProject.Status = currentRow["status"].ToString();

                //insert in the list at last
                resultProyect.AddLast(currentProject);
            } // foreach

            return resultProyect;
        }


        public Proyect GetProyect(string idProyect)
        {

            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetProyect = "GetProyect";
            SqlCommand commandGetProyect = new SqlCommand(slqProcedureGetProyect, conexion);
            commandGetProyect.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            commandGetProyect.Parameters.Add(new SqlParameter("@idProject", idProyect)); //manda el parametro a la bd
            SqlDataAdapter daProyect = new SqlDataAdapter(commandGetProyect);
            DataSet dsProyect = new DataSet();
            daProyect.Fill(dsProyect, "Project");
            conexion.Close();

            DataRowCollection drProyect = dsProyect.Tables["Project"].Rows;
            Proyect proyectFinded = new Proyect();

            foreach (DataRow drActual in drProyect)
            {
                proyectFinded.IdProyect = drActual["idProject"].ToString();
                proyectFinded.Name = drActual["name"].ToString();
                proyectFinded.Status = drActual["status"].ToString();
            }
            return proyectFinded;
        }

        public LinkedList<Proyect> GetProyectsIds()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetProjectIds";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Project");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Project"].Rows;

            LinkedList<Proyect> resultProyect = new LinkedList<Proyect>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Proyect currentProject = new Proyect();
                currentProject.IdProyect = currentRow["idProject"].ToString();
                
                //insert in the list at last
                resultProyect.AddLast(currentProject);
            } // foreach

            return resultProyect;
        }

        public LinkedList<Proyect> GetAllProjectNames()
        {
            SqlConnection connection = new SqlConnection(this.stringConnection);

            string sqlSelect = "GetAllProjectNames";

            SqlDataAdapter sqlDataAdapterClient = new SqlDataAdapter();
            sqlDataAdapterClient.SelectCommand = new SqlCommand();
            sqlDataAdapterClient.SelectCommand.CommandText = sqlSelect;
            sqlDataAdapterClient.SelectCommand.Connection = connection;

            DataSet datasetClients = new DataSet();

            sqlDataAdapterClient.Fill(datasetClients, "Project");
            sqlDataAdapterClient.SelectCommand.Connection.Close();

            DataRowCollection dataRowCollection = datasetClients.Tables["Project"].Rows;

            LinkedList<Proyect> resultProyect = new LinkedList<Proyect>();

            foreach (DataRow currentRow in dataRowCollection)
            {
                Proyect currentProject = new Proyect();
                currentProject.Name = currentRow["name"].ToString();

                //insert in the list at last
                resultProyect.AddLast(currentProject);
            } // foreach

            return resultProyect;
        }

        //se obtienen todos los nombres de proyectos
        //esta vez retornando un DataSet
        public DataSet GetAllProjectNamesDataSet()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetAllProjectNames";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Project");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //GetAllProjectNamesDataSet

        public DataSet GetAllProjectsDataSet()
        {
            SqlConnection sqlConnection = new SqlConnection(stringConnection);
            string query = "GetProyects";

            SqlCommand cmdObtener = new SqlCommand(query, sqlConnection);
            cmdObtener.CommandType = CommandType.StoredProcedure;

            //Se establece la conexion con el adaptador
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmdObtener);

            //Se crea un dataset para guardar los resultados de la consulta
            DataSet dataSet = new DataSet();

            sqlDataAdapter.Fill(dataSet, "Project");

            //cerrar la conexion con el adaptador

            sqlDataAdapter.SelectCommand.Connection.Close();

            return dataSet;
        } //GetAllProjectsDataSet()

        public bool existsTheId(string idProject)
        {
            SqlConnection conexion = new SqlConnection(stringConnection);
            String slqProcedureGetProject = "GetProjectIds";
            SqlCommand commandGetProject = new SqlCommand(slqProcedureGetProject, conexion);
            commandGetProject.CommandType = CommandType.StoredProcedure;
            conexion.Open();
            SqlDataAdapter daProject = new SqlDataAdapter(commandGetProject);
            DataSet dsProject = new DataSet();
            daProject.Fill(dsProject, "Project");
            conexion.Close();

            DataRowCollection drProject = dsProject.Tables["Project"].Rows;

            Proyect currentProject = new Proyect();

            foreach (DataRow drActual in drProject)
            {
                currentProject.IdProyect = drActual["idProject"].ToString();
                if (currentProject.IdProyect.Equals(idProject))
                {
                    return true;
                }//if
            }//foreach
            return false;
        }//existsTheId

    }//class
}//namespace
